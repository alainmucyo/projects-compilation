<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TestController extends Controller
{
    public function index(){
        return view("test");
    }
    public function data(){
        $model=Test::get();
        return DataTables::of(Test::query())->make(true);
    }
}
