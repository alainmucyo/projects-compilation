<?php

namespace App\Http\Controllers;

use App\Resource;
use App\User;

class UserController extends Controller
{
    public function index(){
        $users=User::get();
        return view("index",compact("users"));
    }
    public function add(){
        $resources=Resource::get();
return view("add",compact("resources"));
    }
    public function store(){
        User::create([
            "CustomerId"=>rand(1,1000),
            'Name'=>request("name"),
            "Surname"=>request("surname"),
            "Gender"=>request("gender"),
            "Age"=>request("age"),
            "Region"=>request("region"),
            "JobClassification"=>request("job"),
            "DateJoined"=>request("date"),
            "Balance"=>request("balance")
        ]);
        return back();
    }
    public function delete($id){
        User::where("CustomerID",$id)->delete();
        return back();
    }
}
