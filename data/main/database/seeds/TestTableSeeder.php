<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();
        foreach (range(1,100) as $range){
            \App\Test::create([
                'first_name'=>$faker->firstName,
                'last_name'=>$faker->lastName
            ]);
        }
    }
}
