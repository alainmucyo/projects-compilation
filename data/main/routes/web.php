<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"UserController@index");
Route::get('/add',"UserController@add");
Route::post('/add',"UserController@store");
Route::get("/delete/{id}","UserController@delete");
Route::get("/home","DataController@index");
Route::get("test","TestController@index");
Route::get("test/data","TestController@data");