<html>
<head>
    <title></title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="dataTables.bootstrap4.min.css">
</head>
<body>
<div class="container">
<table class="table" id="table">
    <thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Created At</th>
        <th>Updated At</th>
    </tr>
    </thead>
</table>
</div>
</body>
<script type="text/javascript" src="jquery-3.3.1.js"></script>
<script type="text/javascript" src="datatables.js"></script>
<script type="text/javascript" src="dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="dataTables.select.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#table").DataTable({
            processing:true,
            serverSide:true,
            ajax:'{{ url("test/data") }}',
            columns:[
                {data:'first_name',name:'first_name'},
                {data:'last_name',name:'last_name'},
                {data:'created_at',name:'created_at'},
                {data:'updated_at',name:'updated_at'}
            ]
        });
        $('#table').on('click', 'tbody td:not(:first-child)', function (e) {

            editor.inline(this);
        });
    })
</script>
</html>