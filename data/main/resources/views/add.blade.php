@extends("layout.master")
@section("content")
    <div class="col-sm-9 col-sm-offset-3 col-lg-7 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                        <em class="fa fa-plus"></em>
                    </a></li>
                <li class="active">Add Entry</li>
            </ol>
        </div><!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Entry</h1>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <form method="post" action="/add">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Surname</label>
                        <input type="text" name="surname" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control">
                            <optgroup label="Sex">
                                @foreach($resources as $resource)
                                <option value="{{ $resource->gender }}">{{ $resource->gender }}</option>
                                    @endforeach
                            </optgroup>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Region</label>
                        <select name="region" class="form-control">
                            <optgroup label="Region">
                                @foreach($resources as $resource)
                                    <option value="{{ $resource->region }}">{{ $resource->region }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Job Classification</label>
                        <select name="job" class="form-control">
                            <optgroup label="Job Classification">
                                @foreach($resources as $resource)
                                    <option value="{{ $resource->job_classification }}">{{ $resource->job_classification }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Date Joined</label>
                        <input type="date" name="date" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Ages</label>
                        <input type="number" name="age" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Balance</label>
                        <input type="text" name="balance" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Add Entry" class="btn btn-primary pull-right">
                    </div>
            </form>
        </div>
        </div>
    </div>

@endsection