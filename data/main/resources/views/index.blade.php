@extends("layout.master")
@section("content")
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div><!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
        </div>
        <table id="dataTable">
            <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Surname</td>
                <td>Gender</td>
                <td>Age</td>
                <td>Region</td>
                <td>Job classification</td>
                <td>Date joined</td>
                <td>Balance</td>
                <td>Edit</td>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->CustomerID }}</td>
                    <td>{{ $user->Name }}</td>
                    <td>{{ $user->Surname }}</td>
                    <td>{{ $user->Gender }}</td>
                    <td>{{ $user->Age }}</td>
                    <td>{{ $user->Region }}</td>
                    <td>{{ $user->JobClassification }}</td>
                    <td>{{ $user->DateJoined }}</td>
                    <td>{{ $user->Balance }}</td>
                    <td><a href="/delete/{{ $user->CustomerID }}" class="btn btn-danger" onclick="return confirm('You Are About To Delete User')"><span class="fa fa-recycle"></span></a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection