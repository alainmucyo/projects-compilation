<div class="row">
    @foreach($messages as $message)
        @php
            if ($message->user_id==auth()->user()->id){
            $bg_color="bg-info";
            $float1="col-md-3 col-sm-3";
            $float2="col-md-9 col-sm-9";
            $text_color="text-white";
            $mute="";
            }
            else{
            $bg_color="bg-light";
            $float1="";
            $float2="col-md-9 col-sm-9";
             $text_color="";
             $mute="text-muted";
            }
        \App\Message::find($message->id)->where("receiver_id",auth()->user()->id)->update(["status"=>0]);
        @endphp

        <div class="{{ $float1 }}"> &nbsp;</div>
        <div class="{{ $float2 }}" style="margin-bottom: 3%">
            <div class="card {{ $bg_color }} {{ $text_color }} p-1">
                {{ $message->content }} <br><br><br>
                <small class="{{ $mute }} text-dark">{{ $message->created_at->diffForHumans() }}</small>
                <small class="{{ $mute }} text-dark text-right">{{ $message->status? "Seen":"Sent" }}</small>

            </div>
        </div>
    @endforeach
</div>