@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Available Users</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                <div class="list-group">
                    @foreach($users as $user)
                        @php
                         $mess_num=\App\User::find(auth()->user()->id)->messages->where("receiver_id",$user->id)->where("status",0)->count();
                        @endphp
                    <a class="list-group-item list-group-item-action" href="{{ url('chat/'.$user->id) }}">{{ $user->name }}
                    <span class="float-right"><span class="badge badge-info">{{ $mess_num }}</span></span>
                    </a>
                        @endforeach
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
