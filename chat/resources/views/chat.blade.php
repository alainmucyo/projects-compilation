@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card" >
                    <div class="card-header">{{ $user->name }}</div>
                    <div class="card-body" style="height: 550px;overflow: scroll; overflow-x: hidden">
                        <div id="main">
                        <div class="row">
                            @foreach($messages as $message)
                                @php
                                if ($message->sender_id==auth()->user()->id){
                                $bg_color="bg-info";
                                $float1="col-md-3 col-sm-3";
                                $float2="col-md-9 col-sm-9";
                                $text_color="text-white";
                                $mute="";
                                }
                                else{
                                $bg_color="bg-light";
                                $float1="";
                                $float2="col-md-9 col-sm-9";
                                 $text_color="";
                                 $mute="text-muted";
                                }
                                        @endphp

                                <div class="{{ $float1 }}"> &nbsp;</div>
                                <div class="{{ $float2 }}" style="margin-bottom: 3%">
                                    <div class="card {{ $bg_color }} {{ $text_color }} p-1">
                                        {{ $message->content }} <br><br><br>
                                        <small class="{{ $mute }} text-dark">{{ $message->created_at->diffForHumans() }}</small>
                                        <small class="{{ $mute }} text-dark text-right">{{ $message->status? "Seen":"Sent" }}</small>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                        </div>
                        <hr>
                        <form method="post" id="form" action="" class="form-horizontal" role="form">
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                            <textarea name="content" id="content" class="form-control" cols="30"
                                                      rows="2"></textarea>
                                </div>
                                <input type="hidden" value="{{ $user->id }}" id="user_id">
                                {{ csrf_field() }}
                                <div class="form-group col-md-2">
                                    <button type="submit" class="btn btn-primary float-right">SEND
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
<script src="{{ asset('js/jquery.js') }}" ></script>
<script type="text/javascript">
    function messages() {
        $(function () {
            var user_id=$("#user_id").val();
            $.get("/messages/"+user_id,function (data) {
                $("#main").html(data);
            });
        });
    }
    setInterval(messages,1000);
    $(function () {
       $("#form").submit(function (e) {
           e.preventDefault();
            var content=$("#content").val();
            var user_id=$("#user_id").val();
            if (content.trim()==""){
                return false;
            }
            $.post("/send/"+user_id, $("#form").serialize(),function (data) {
                if (data=="ok"){
                    $("#content").val("");
                    messages();
                }
            });
       })
    });

</script>