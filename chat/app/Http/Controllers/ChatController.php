<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
      $messages = Message::where("user_id", auth()->user()->id)->where("receiver_id", $id)->orWhere("user_id", $id)->where("receiver_id", auth()->user()->id)->get();

        return view("chat", compact("user","messages"));
    }

    public function send($id)
    {
      //  $user = User::find($id);
        Message::create([
            "content" => \request("content"),
            "user_id" => auth()->user()->id,
            "receiver_id" => $id,
            "status" => 0
        ]);
        return "ok";
      //  return view("chat", compact("user"));
    }
    public function messages($id){
        $user = User::find($id);
        $messages = Message::where("user_id", auth()->user()->id)->where("receiver_id", $id)->orWhere("user_id", $id)->where("receiver_id", auth()->user()->id)->get();
        return view("messages", compact("user","messages"));

    }
}
