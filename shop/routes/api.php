<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource("/brands", 'BrandController');
Route::apiResource("/category", 'CategoryController');
Route::apiResource("/size", 'SizeController');
Route::apiResource("/items", 'ItemController');
Route::apiResource("/stock","StockController");
Route::apiResource("/users","UsersController");
Route::get("/sales/main","SaleController@main");
Route::post("/profile/{user}","UsersController@profile");
Route::apiResource("/sales","SaleController");

Route::post("/login","AuthController@login");
Route::post("/register","AuthController@register");
Route::get("/weekly","SaleController@weekly");
Route::get("/monthly","SaleController@monthly");