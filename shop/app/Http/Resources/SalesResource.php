<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesResource extends JsonResource
{

    public function toArray($request)
    {
        return [
          "id"=>$this->id,
            "client"=>$this->client,
            "user"=>$this->user,
            "item"=>$this->item,
            "quantity"=>$this->quantity,
            "created_at"=>$this->created_at
        ];
    }
}
