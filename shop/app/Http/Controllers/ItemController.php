<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AdminMiddleware;
use App\Http\Resources\ItemResource;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ItemController extends Controller
{
     function __construct()
     {
         $this->middleware("auth:api");
     }

    public function index()
    {
        return ItemResource::collection(Item::all());
    }


    public function store(Request $request)
    {

        $validators = validator()->make($request->all(), [
            'name' => 'required|unique:items',
            'price'=>'required|numeric',
            'brand'=>'required',
            'size'=>'required',
            'category'=>'required'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }

        return new ItemResource(Item::create([
            "name"=>$request['name'],
            "price"=>$request['price'],
            "brand_id"=>$request->brand['id'],
            "size_id"=>$request->size['id'],
            "category_id"=>$request->category['id'],
        ]));
    }


    public function update(Request $request, Item $item)
    {
        $validators = validator()->make($request->all(), [
            'name' => 'required|unique:items,name,'.$item->id,
            'price'=>'required|numeric',
            'brand'=>'required',
            'size'=>'required',
            'category'=>'required'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }

     $item->update([
            "name"=>$request['name'],
            "price"=>$request['price'],
            "brand_id"=>$request->brand['id'],
            "size_id"=>$request->size['id'],
            "category_id"=>$request->category['id'],
        ]);
        return new ItemResource($item);
    }


    public function destroy(Item $item)
    {
        $item->delete();
        return \response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
