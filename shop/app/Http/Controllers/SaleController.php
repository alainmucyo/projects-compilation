<?php

namespace App\Http\Controllers;

use App\Http\Resources\ItemResource;
use App\Http\Resources\SalesResource;
use App\Http\Resources\MainSale;
use App\Item;
use App\Sale;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SaleController extends Controller
{

    // function __construct()
    // {
    //     $this->middleware("auth:api");
    // }

    public function index()
    {
        return SalesResource::collection(Sale::all());
    }


    public function store(Request $request)
    {

        foreach ($request->all() as $item) {
            $items = Item::find($item['item_id'])->stock->quantity;
            if ($items >= $item['quantity']) {
                Item::find($item['item_id'])->stock->update(["quantity" => $items - $item['quantity']]);
                Sale::create($item);
            }
        }
    }


    public function main()
    {
        $items = Item::with("sales", "category", "stock", "size")->get()->filter(function ($item) {
            return $item->sales->count() > 0;
        });
        return MainSale::collection($items);
    }

    //reverse
    public function show(Sale $sale)
    {

        $item = $sale->item;
        $item->stock->update(["quantity" => $item->stock->quantity + \request()->quantity]);
        $sale->delete();
        return new ItemResource($item);
    }

    public function destroy(Sale $sale)
    {
        $sale->delete();
    }

    public function weekly()
    {
        $days = array();
        $test = [0, 1, 2, 3, 4, 5, 6];
        foreach ($test as $i) {
            $test1 = (int)2;
            array_push($days, Sale::whereBetween("created_at", [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->whereRaw('WEEKDAY(sales.created_at)=' . $i)->sum("quantity"));
        }

        return $days;
    }

    public function monthly()
    {
        $months = array();
        $data = Sale::whereBetween("created_at", [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->get();
        for ($i = 1; $i <= 12; $i++) {
            array_push($months, Sale::whereMonth("created_at", $i)->sum("quantity"));
        }
        return $months;

    }

}
