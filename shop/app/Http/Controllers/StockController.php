<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AdminMiddleware;
use App\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
function __construct()
{
    $this->middleware(["auth:api",AdminMiddleware::class]);
}

    public function index()
    {
        return Stock::with("stock")->get();
    }


    public function store(Request $request)
    {
        $request->validate([
            "quantity" => "required",
            "item_id"=>"required"
        ]);
       return Stock::create($request->all());
    }


    public function show(Stock $stock)
    {
        return $stock;
    }


    public function update(Request $request, Stock $stock)
    {
        $request->validate([
            "quantity" => "required",
        ]);
        $stock->update($request->all());
        return $stock;
    }


    public function destroy(Stock $stock)
    {
        $stock->delete();
    }
}
