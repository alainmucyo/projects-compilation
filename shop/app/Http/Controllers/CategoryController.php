<?php

namespace App\Http\Controllers;


use App\Category;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
   
 function __construct()
    {
        $this->middleware(["auth:api",AdminMiddleware::class]);
    }
    public function index()
    {

        return CategoryResource::collection(Category::all());
    }


    public function store(Request $request)
    {
        $validators = validator()->make($request->all(), [
            'name' => 'required|unique:categories'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }
        return new CategoryResource(Category::create($request->all()));
    }


    public function update(Request $request, Category $category)
    {
        $validators = validator()->make($request->all(), [
            'name' => 'required|unique:categories'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }

     $category->update($request->all());
        return $category;
    }


    public function destroy(Category $category)
    {
        $category->delete();
        return \response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
