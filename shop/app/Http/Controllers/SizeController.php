<?php

namespace App\Http\Controllers;


use App\Http\Middleware\AdminMiddleware;
use App\Http\Resources\SizeResource;
use App\Size;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SizeController extends Controller
{
    function __construct()
    {
        $this->middleware(["auth:api",AdminMiddleware::class]);
    }

    public function index()
    {
        return SizeResource::collection(Size::all());
    }


    public function store(Request $request)
    {
     
        $validators = validator()->make($request->all(), [
            'size' => 'required|unique:sizes'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }
        return new SizeResource(Size::create($request->all()));
    }


    public function update(Request $request, Size $size)
    {
      
        $validators = validator()->make($request->all(), [
            'size' => 'required|unique:sizes'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }

        $size->update($request->all());
        return $size;
    }


    public function destroy(Size $size)
    {
        $size->delete();
        return \response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
