<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller
{
    public function params($username, $password)
    {
        return $params = [
            "grant_type" => "password",
            "client_id" => "2",
            "client_secret" => "HuVofYi3ULNY9iCP3mN7kAVk5Hh1pSNXwYiHc6kG",
            "username" => $username,
            "password" => $password,
            "scope" => "*"
        ];
    }

    public function login(Request $request)
    {
        $params = $this->params($request->username, $request->password);
        $request->request->add($params);
        $proxy = Request::create("oauth/token", "POST");
        return Route::dispatch($proxy);
    }

}
