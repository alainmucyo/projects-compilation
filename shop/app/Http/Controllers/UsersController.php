<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AdminMiddleware;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    function __construct()
    {
        $this->middleware("auth:api");
        $this->middleware([AdminMiddleware::class])->except("profile");
    }

    public function index()
    {
        return User::all();
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users'],
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => $request['type'],
            'password' => Hash::make('a'),
        ]);
        return $user;
    }


    public function show(User $user)
    {
        return $user;
    }


    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users,email,' . $user->id],

        ]);
        $user->update($request->all());
        return $user;
    }

    public function destroy(User $user)
    {
        $user->delete();
    }

    public function profile(Request $request, User $user)
    {

       $validators = validator()->make($request->all(),[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users,email,' . $user->id],

        ]);
           if ($validators->fails()) {
            return response()->json($validators->errors(), 422);
        }
        $avatar = $user->avatar;
        $user->update(["name" => $request->name, "email" => $request->email]);
        if ($request->hasFile("avatar")) {
            if ($avatar != null && file_exists(public_path($avatar))) {
                unlink(public_path($avatar));
            }
            $request->avatar->store("avatar");
            $user->update(["avatar" => $request->avatar->store("avatar")]);

        }
        if ($request->old_password && trim($request->old_password) != "") {

            $old_password = $user->getAuthPassword();
            if (password_verify($request->old_password, $old_password)) {
                $validators = validator()->make($request->all(),[
                    "password" => "required|confirmed"
                ]);
                 if ($validators->fails()) {
            return response()->json($validators->errors(), 422);
        }
                $user->update(["password" => Hash::make($request->password)]);
            }
            else{
              return response()->json(["old_password"=>"Wrong Password"],400);

            }
        }

        return $user;
       
    }
}
