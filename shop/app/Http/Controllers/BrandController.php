<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Resources\BrandResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BrandController extends Controller
{
    function __construct()
    {
        $this->middleware(["auth:api",AdminMiddleware::class]);
    }

    public function index()
    {
        return BrandResource::collection(Brand::all());
    }


    public function store(Request $request)
    {
        $validators = validator()->make($request->all(), [
            'name' => 'required|unique:brands'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }
        return new BrandResource(Brand::create($request->all()));
    }


    public function update(Request $request, Brand $brand)
    {
        $validators = validator()->make($request->all(), [
            'name' => 'required|unique:brands'
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(), Response::HTTP_BAD_REQUEST);
        }

     $brand->update($request->all());
        return $brand;
    }


    public function destroy(Brand $brand)
    {
        $brand->delete();
        return \response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
