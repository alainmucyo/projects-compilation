<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class UserMiddleware
{

    public function handle($request, Closure $next)
    {
        if(auth()->user()->type != 0)
            return response()->json(["message"=>"Unauthenticated."],Response::HTTP_UNAUTHORIZED);
        return $next($request);
    }
}
