<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'brand_id', 'category_id', 'size_id', 'price'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function stock()
    {
        return $this->hasOne(Stock::class);
    }

    public function sales(){
        return $this->hasMany(Sale::class);
    }
}
