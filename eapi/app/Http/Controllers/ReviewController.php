<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReviewResource;
use App\Product;
use App\Review;
use Illuminate\Http\Request;
use Barryvdh\Cors\HandleCors;
use Symfony\Component\HttpFoundation\Response;

class ReviewController extends Controller
{
      public function __construct()
    {
    //  $this->middleware(HandleCors::class);
    }

    public function index(Product $product)
    {
        return ReviewResource::collection($product->reviews);
    }


    public function create()
    {
        //
    }


    public function store(Request $request, $product)
    {
        $request->validate([
            "customer"=>"required",
            "star"=>"required|numeric",
            "review"=>"required"
        ]);

        $request["product_id"]=$product;
        $review=Review::create($request->all());
        return new ReviewResource($review);
    }


    public function show($product,$review)
    {
    
      return Review::find($review);
    return response()->json(null, Response::HTTP_NO_CONTENT); 
    }


    public function edit(Review $review)
    {
        //
    }


    public function update(Request $request, Review $review,$product)
    {
        $review->update($request->all());
        return new ReviewResource($review);

    }


    public function destroy($product,$review)
    {
    
        Review::find($review)->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT); 
    }
}
