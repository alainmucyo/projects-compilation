<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Product;
use Barryvdh\Cors\HandleCors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function __construct()
    {
     //  $this->middleware(HandleCors::class);
    }

    public function index()
    {
  
        return ProductCollection::collection(Product::all());
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {


        $validator=validator()->make($request->all(),[
            "name" => "required|unique:products",
            "description" => "required",
            "stock" => "required|numeric",
            "discount" => "required|numeric",
            "price" => "required|numeric"
        ]);
        if ($validator->fails()){
            return \response()->json(["errors"=>$validator->errors()],200);
       }
       // $request['user_id'] = Auth::user()->id;
         $request['user_id'] = 1;
        $request["detail"] = $request["description"];
        $request["stick"] = $request["stock"];
        unset($request["description"]);
        unset($request["stock"]);
        $product = Product::create($request->all());
    
           return new  ProductResource($product);
    }


    public function show(Product $product)
    {
       
        return new ProductResource($product);
    }


    public function edit(Product $product)
    {
        //
    }


    public function update(Request $request, Product $product)
    {

      /*  if ($product->user_id !== Auth::user()->id) {
          return  response()->json(["error" => "Product doesn't belong to You"]);

        } */


        if ($request["description"]) {
            $request["detail"] = $request["description"];
            unset($request["description"]);
        }
        if ($request["stock"]) {
            $request["stick"] = $request["stock"];
            unset($request["stock"]);
        }
        $product->update($request->all());
        return new  ProductResource($product);
    }


    public function destroy(Product $product)
    {
      /*  if ($product->user_id !== Auth::user()->id) {
           return response()->json(["error" => "Product doesn't belong to You"]);

        } else { */

       
           
           $product->delete();
            return response()->json(null, Response::HTTP_NO_CONTENT);
      //  }

    }
}
