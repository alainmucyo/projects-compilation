<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "name" => $this->name,
            "price" => $this->price,
            "stock" => $this->stick,
            "description"=>$this->detail,
            "discount" => $this->discount,
            "addedBy" => $this->user->name,
            "rating" => $this->reviews->count() == 0 ? "No rating yet" : round($this->reviews->sum("star") / $this->reviews->count(),2),
            "totalPrice"=> (100-$this->discount)*$this->price,
            "reviews"=>[
                "href"=>route("reviews.index",$this->id)
            ]
        ];
    }
}
