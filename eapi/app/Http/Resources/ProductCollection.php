<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            "id" => $this->id,
            "name" => $this->name,
            "addedBy" => $this->user->name,
            "rating" => $this->reviews->count() == 0 ? "No rating yet" : round($this->reviews->sum("star") / $this->reviews->count(),2),
            "totalPrice"=> (100-$this->discount)*$this->price,
            "more"=>[
                "href"=>route("products.show",$this->id)
            ]

        ];
    }
}
