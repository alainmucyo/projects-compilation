<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        "name"=>$faker->word,
        "detail"=>$faker->paragraph,
        "price"=>$faker->numberBetween(10,1000),
        "stick"=>$faker->randomDigit,
        "discount"=>$faker->numberBetween(0,30),
        "user_id"=>function(){
        return \App\User::all()->random();
        }
    ];
});
