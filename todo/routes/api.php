<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("/login", "AuthController@login");
Route::post("/register", "AuthController@register");
Route::apiResource("/todo", "TodoController");

Route::get("/hello", function () {
    return "hello";
});