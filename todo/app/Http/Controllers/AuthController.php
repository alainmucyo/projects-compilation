<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function params($username, $password)
    {
        return $params = [
            "grant_type" => "password",
            "client_id" => "2",
            "client_secret" => "2XMIFFuYXDZ9Mz1FG7BqQ9FxI1hcosYdh3Xcb3Rt",
            "username" => $username,
            "password" => $password,
            "scope" => "*"
        ];
    }

    public function login(Request $request)
    {
        $params = $this->params($request->username, $request->password);
        $request->request->add($params);
        $proxy = Request::create("oauth/token", "POST");
        return Route::dispatch($proxy);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
        ]);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        $params = $this->params($request->email, $request->password);
        $request->request->add($params);
        $proxy = Request::create("oauth/token", "POST");
        return Route::dispatch($proxy);
    }
}
