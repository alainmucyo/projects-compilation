<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    function __construct()
    {
        $this->middleware("auth:api");
    }

    public function index()
    {
        return auth()->user()->todos;
    }

    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "description"=>"required",
            "date"=>"required",
        ]);

        $request['user_id']=auth()->user()->id;
        $todo=Todo::create($request->all());
        return $todo;
    }



    public function show(Todo $todo)
    {
        return $todo;
    }

    public function update(Request $request, Todo $todo)
    {
        $request->validate([
            "name"=>"required",
            "description"=>"required",
            "date"=>"required",
        ]);
        $todo->update($request->all());
        return $todo;
    }

    public function destroy(Todo $todo)
    {
        $todo->delete();
    }
}
