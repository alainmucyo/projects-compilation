@extends("layouts.master")
@section("title","Home Page")
    @section("unwanted","none")
@section("contents")

    <section class="engine"><a href="https://mobiri.se/o">free portfolio site templates</a></section>
    <section class="carousel slide cid-r4PGCmpqtQ" data-interval="false" id="slider1-0">


        <div class="full-screen">
            <div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="carousel"
                 data-interval="4000">
                <ol class="carousel-indicators">
                    <li data-app-prevent-settings="" data-target="#slider1-0" class=" active" data-slide-to="0"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-0" data-slide-to="1"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-0" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false"
                         style="background-image: url(assets/images/1.jpg);">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div>
                                <img src="assets/images/1.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-center"><h2 class="mbr-fonts-style display-1">FULL SCREEN
                                            SLIDER</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">Choose from the large
                                            selection of latest pre-made blocks - jumbotrons, hero images, parallax
                                            scrolling, video backgrounds, hamburger menu, sticky header and more.</p>
                                        <div class="mbr-section-btn" buttons="0"><a class="btn display-4 btn-primary"
                                                                                    href="https://mobirise.com">GET
                                                STARTED</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false"
                         style="background-image: url(assets/images/2.jpg);">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div>
                                <img src="assets/images/2.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-left"><h2 class="mbr-fonts-style display-1">VIDEO
                                            SLIDE</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">Slide with youtube video
                                            background and color overlay. Title and text are aligned to the left.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false"
                         style="background-image: url(assets/images/3.jpg);">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div>
                                <img src="assets/images/3.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-right"><h2 class="mbr-fonts-style display-1">IMAGE
                                            SLIDE</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">Choose from the large
                                            selection of latest pre-made blocks - jumbotrons, hero images, parallax
                                            scrolling, video backgrounds, hamburger menu, sticky header and more.</p>
                                        <div class="mbr-section-btn" buttons="0"><a class="btn btn-info display-4"
                                                                                    href="https://mobirise.com">FOR
                                                WINDOWS</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button"
                   data-slide="prev" href="#slider1-0"><span aria-hidden="true"
                                                             class="mbri-left mbr-iconfont"></span><span
                            class="sr-only">Previous</span></a><a data-app-prevent-settings=""
                                                                  class="carousel-control carousel-control-next"
                                                                  role="button" data-slide="next"
                                                                  href="#slider1-0"><span aria-hidden="true"
                                                                                          class="mbri-right mbr-iconfont"></span><span
                            class="sr-only">Next</span></a></div>
        </div>

    </section>

    <section class="mbr-section info1 cid-r4Qds4wiT4" id="info1-q">


        <div class="container">
            <div class="row justify-content-center content-row">
                <div class="media-container-column title col-12 col-lg-7 col-md-6">
                    <h3 class="mbr-section-subtitle align-left mbr-light pb-3 mbr-fonts-style display-5">
                        Receive 500 rwf when you found license</h3>
                    <h2 class="align-left mbr-bold mbr-fonts-style display-2">
                        QUICK GAIN MONEY</h2>
                </div>
                <div class="media-container-column col-12 col-lg-3 col-md-4">
                    <div class="mbr-section-btn align-right py-4">
                        <a class="btn btn-primary display-4" href="https://mobirise.com">SEE LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section info2 cid-r4PTk0HVkh" id="info2-g">


        <div class="container">
            <div class="row main justify-content-center">
                <div class="media-container-column col-12 col-lg-3 col-md-4">
                    <div class="mbr-section-btn align-left py-4"><a class="btn btn-primary display-7"
                                                                    href="https://mobirise.com">
                            <span class="mbri-preview mbr-iconfont"></span>FIND YOURS &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</a>
                    </div>
                </div>
                <div class="media-container-column title col-12 col-lg-7 col-md-6">
                    <h2 class="align-right mbr-bold mbr-white pb-3 mbr-fonts-style display-2">CHEAP AND EASY
                        PAYMENT</h2>
                    <h3 class="mbr-section-subtitle align-right mbr-light mbr-white mbr-fonts-style display-5">
                        PAY ONLY 1000RWF FOR YOUR LICENSE</h3>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section article content12 cid-r4PYHDrJby" id="content12-h">


        <div class="container">
            <div class="media-container-row">
                <div class="mbr-text counter-container col-12 col-md-8 mbr-fonts-style display-7">
                    <ul>
                        <li><strong>LOST LICENSE</strong> - Navigate in all found license and check whether there is
                            yours. If found you will asked to pay 100 Rwf by MTN mobile money and get location, contacts
                            for someone who found it&nbsp;<a href="https://mobirise.com/">Try it now!</a></li>
                        <li><strong>FOUND LICENSE</strong>&nbsp;- Login and there will be form where you will provide
                            all requirements and proofs for that license and provide your phone number where you will
                            receive money. <a href="https://mobirise.com/">Try it now!</a></li>
                        <li><strong>ACCESSING</strong> - For accessing all these services, you have to create account by
                            providing names and other address and provide MTN phone number which are registered in
                            mobile money. <a href="https://mobirise.com/">Try it now!</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection