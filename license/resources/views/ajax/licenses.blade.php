<div class="row">
    @foreach($licenses as $license)
        <div class="card p-3 col-12 col-md-6 col-lg-4">
            <div class="card-wrapper">
                <div class="card-img">
                    <img src="{{ Storage::url($license->image) }}" alt="Mobirise">
                </div>
                <div class="card-box">
                    <h4 class="card-title mbr-fonts-style display-7">
                        {{ $license->names }}
                    </h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                        <b>{{ $license->category }}ID</b> - {{ $license->more }}.<br>
                        <span class="text-muted float-right">{{ $license->created_at->diffForHumans() }}</span>

                    </p>
                </div>
                <div class="mbr-section-btn text-center">

                    <a href="{{ url("/this_license/".$license->id) }}" class="btn btn-primary display-4">
                        Learn More
                    </a>
                </div>

            </div>
        </div>
    @endforeach

</div>
</div>
<div class="row justify-content-center">
    {!! $licenses->links() !!}
</div>