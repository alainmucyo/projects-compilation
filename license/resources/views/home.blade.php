@extends("layouts.master")
@section("title","Found Licenses")
@section("contents")
    <section class="features3 cid-r4RvhB6i8O mbr-parallax-background" id="features3-u">

        <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(239, 239, 239);">
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <select class="form-control" id="sort">
                        <optgroup label="Categories">
                            <option value="0">All</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->name }}">{{ $category->name }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
                <div class="col-md-8">
                    <form id="search" onsubmit="event.preventDefault();" class="form form-horizontal ">

                        <div class="form-row">
                            <div class="form-group  col-md-6 col-sm-12">
                                <input type="search" class="form-control" id="content" placeholder="Search For License">

                            </div>
                            <div class=" form-group-sm col-md-3 col-sm-12">
                                <button class="btn btn-primary display-7 btn-block btn-sm" type="submit"
                                        style="margin-top: -0.0%;margin-left: 0%">     <span
                                            class="mbri-search mbr-iconfont mbr-iconfont-btn"></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                @foreach($licenses as $license)
                    <div class="card p-3 col-12 col-md-6 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <img src="{{ Storage::url($license->image) }}" alt="Image">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-7">
                                    <b>{{ $license->names }}</b>
                                </h4>
                                <p class="mbr-text mbr-fonts-style display-7">
                                    <b>{{ $license->category->name }}</b> - {{ $license->more }}.<br>
                                    <span class="text-muted float-right">{{ $license->created_at->diffForHumans() }}</span>

                                </p>
                            </div>
                            <div class="mbr-section-btn text-center">

                                <a href="{{ url("/this_license/".$license->id) }}" class="btn btn-primary display-4">
                                    Learn More
                                </a>
                            </div>

                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row justify-content-center">
                {!! $licenses->links() !!}
            </div>
        </div>
    </section>
@endsection
<script src="{{ asset('assets/web/assets/jquery/jquery.min.js') }}"></script>

<script type="text/javascript">

    $(function () {
         $("#sort").val("{{ $selected }}").prop("selected",true);
        $("#sort").change(function () {
            var selected=$(this).val();
            if (selected != 0) {
                window.location = "/license/cat/" + selected;
            }else{
                window.location='/home';
            }
        });
        $("#search").submit(function () {
            var search = $("#content").val();

            window.location = "/license/find/" + search;
        });

    })
</script>
