@extends("layouts.master")
@section("title","Found Licenses")
@section("contents")
    <section class="engine"><a href="https://mobiri.se/c">free website builder</a></section>
    <section class="mbr-gallery mbr-slider-carousel cid-r59ouKv8uC" id="gallery3-1g">
        <div>
            <div><!-- Filter --><!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                @foreach($images as $image)
                                    <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false"
                                         data-tags="Awesome">
                                        <div href="#lb-gallery3-1g" data-slide-to="{{ $i }}" data-toggle="modal">
                                            <img src="{{ Storage::url($image->image) }}" alt="" title=""><span
                                                    class="icon-focus"></span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div><!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1"
                     data-keyboard="true" data-interval="false" id="lb-gallery3-1g">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <ol class="carousel-indicators">
                                    @foreach($images as $image)
                                    <li data-app-prevent-settings="" data-target="#lb-gallery3-1g" id="my-carousel"
                                        data-slide-to="{{ $b }}"></li>
@endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @foreach($images as $image)
                                    <div class="carousel-item" id="my-carousel-item">
                                        <img src="{{ Storage::url($image->image) }}" alt="" title="">
                                    </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev"
                                   href="#lb-gallery3-1g"><span class="mbri-left mbr-iconfont"
                                                                aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a>
                                <a class="carousel-control carousel-control-next" role="button" data-slide="next"
                                   href="#lb-gallery3-1g"><span class="mbri-right mbr-iconfont"
                                                                aria-hidden="true"></span><span
                                            class="sr-only">Next</span></a>
                                <a class="close" href="#" role="button" data-dismiss="modal"><span
                                            class="sr-only">Close</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
