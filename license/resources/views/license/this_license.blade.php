@extends("layouts.master")
@section("title","Found Licenses")
@section("contents")
    <section class="tabs4 cid-r4V1o1C4N9" id="tabs4-14">


        <div class="container">
            <h4 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
                {{ $license->names }}'s {{ $license->category->name }}
            </h4>


            <div class="media-container-row mt-5 pt-3">
                <div class="mbr-figure" style="width: 60%;">
                    <img src="{{ Storage::url($license->image) }}" alt="IMAGE">
                </div>
                <div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active mbr-fonts-style display-7" role="tab" data-toggle="tab"
                               href="#tab1">
                                ABOUT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mbr-fonts-style display-7" role="tab" data-toggle="tab" href="#tab2">
                                PAYMENT
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab1" class="tab-pane in active" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="mbr-text py-5 mbr-fonts-style display-4">
                                    <p><b>Owner's Names: </b> {{ $license->names }}</p>
                                    <p><b>Category: </b>{{ $license->category->name }}</p>
                                    <p><b>About: </b>{{ $license->more }}</p>
                                    <p><b>Found : </b>{{ $license->created_at->diffForHumans() }}</p>
                                    <p>For Obtaining It Go To Payment </p>
                                    <p><a href="{{ url("/license/gallery/".$license->id) }}">More Images...</a></p>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="mbr-text py-5 mbr-fonts-style display-4">
                                       Please, fill with your MTN mobile money phone number and make sure you have X rwf on it!
                                    <form>
                                        <div class="form-group">
                                        <input type="text"
                                               class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                               data-form-field="Phone Number" required min="100000000" minlength="10"
                                               max="1000000000"
                                               name="phone" placeholder="* 0789999999" id="phone-form1-l"
                                               value="{{ auth()->user()->phone }}"/>
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('phone') }}</strong></small>
                                    </span>
                                        @endif
                                        </div>
                                        <span class=" input-group-btn"><button href="" type="submit"
                                                                              class="btn btn-primary btn-form display-4">PAY !</button></span>
                                    </form>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection