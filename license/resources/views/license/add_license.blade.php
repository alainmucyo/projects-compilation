@extends('layouts.master')
@section("title","Add License Form")
@section('contents')
    <section class="engine"><a href="https://mobiri.se">Mobirise</a></section>
    <section class="tabs2 cid-r4W3ReBalB" id="tabs2-17">


        <div class="container">
            <h2 class="mbr-section-title align-center pb-5 mbr-fonts-style display-2">
                ADD AND MANAGES LICENSES
            </h2>
            <div class="media-container-row">
                <div class="col-12 col-md-8">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active mbr-fonts-style display-7" role="tab" data-toggle="tab"
                               href="#tab1">
                                ADD LICENSE
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mbr-fonts-style display-7" role="tab" data-toggle="tab" href="#tab2">
                                ADDED LICENSE
                            </a>
                        </li>


                    </ul>
                    <div class="tab-content">
                        <div id="tab1" class="tab-pane in active" role="tabpanel">

                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="title col-12 col-lg-8">
                                        <h3 class="mbr-section-title align-center pb-3 mbr-fonts-style display-4">ADD
                                            LICENSE FORM
                                        </h3>
                                        <h6 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-6">
                                            Easily add license that you found by providing all required details
                                            correctly.
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <form class="mbr-form" action="{{ url("/add_license") }}"
                                          enctype="multipart/form-data" method="post"
                                          data-form-title="Add License Form"><input type="hidden" name="email"
                                                                                    data-form-email="true"
                                                                                    value="2TQR4o1GT7yT8PBHeqVhyabAJmpuZ0oVXfMUGyGWz+SCh/uZh0j0wyFm3MZYYAum5p+T3VHAvfPe7PLqlsqG47a9+0jna9apF4hDHuLTwDAXq5AabQ613WeOJkWQwpk7">
                                        <div class="row row-sm-offset">
                                            {{ csrf_field() }}
                                            <div class="col-md-6 multi-horizontal" data-for="name">
                                                <div class="form-group">
                                                    <label class="form-control-label mbr-fonts-style display-7"
                                                           for="name-form1-l">Owner's
                                                        Names</label>
                                                    <input type="text" class="form-control" name="names"
                                                           data-form-field="Name"
                                                           required="" id="name-form1-l" value="{{ old("names") }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 multi-horizontal" data-for="phone">
                                                <div class="form-group">
                                                    <label class="form-control-label mbr-fonts-style display-7"
                                                           for="category-form1-l">Category</label>
                                                    <select class="form-control" name="category"
                                                            data-form-field="Category" required
                                                            id="category-form1-l">
                                                        <optgroup label="Select Category">
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row row-sm-offset">
                                            <div class="col-md-8 multi-horizontal" data-for="name">
                                                <div class="form-group">
                                                    <label class="form-control-label mbr-fonts-style display-7"
                                                           for="name-form1-l">Address
                                                        Of Where It Is</label>
                                                    <input type="text" class="form-control" name="address"
                                                           data-form-field="Name"
                                                           required="" id="name-form1-l" value="{{ old("address") }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 multi-horizontal" data-for="phone">
                                                <div class="form-group">
                                                    <label class="form-control-label mbr-fonts-style display-7"
                                                           for="phone-form1-l">Phone
                                                        Number(MTN)</label>
                                                    <input type="tel"
                                                           class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                                           data-form-field="Phone Number" required min="100000000"
                                                           minlength="10"
                                                           max="1000000000"
                                                           name="phone" placeholder="* 0789999999" id="phone-form1-l"
                                                           value="{{ old("phone") }}"/>
                                                    @if ($errors->has('phone'))
                                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('phone') }}</strong></small>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row row-sm-offset">
                                            <div class="col-md-8 multi-horizontal" data-for="more">
                                                <div class="form-group">
                                                    <label class="form-control-label mbr-fonts-style display-7"
                                                           for="more-form1-l">More
                                                        About License</label>
                                                    <input type="text" class="form-control" name="more"
                                                           data-form-field="More"
                                                           required="" id="more-form1-l" value="{{ old("more") }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 multi-horizontal" data-for="phone">
                                                <div class="form-group">
                                                    <label class="form-control-label mbr-fonts-style display-7"
                                                           for="phone-form1-l">License
                                                        Image</label>
                                                    <input type="file" class="form-control"
                                                           data-form-field="Phone Number"
                                                           accept="image/*" name="image" required id="phone-form1-l">
                                                    @if ($errors->has('image'))
                                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('image') }}</strong></small>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <span class="input-group-btn"><button href="" type="submit"
                                                                              class="btn btn-primary btn-form display-4">ADD LICENSE</button></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane" role="tabpanel">
                            <div class="title col-12 col-lg-8">
                                <h3 class="mbr-section-title align-center pb-3 mbr-fonts-style display-4">ADD
                                    MORE IMAGES TO LICENSE
                                </h3>
                                <h6 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-6">
                                    If you have other images for license, please add them.
                                </h6>
                            </div>
                            <div class="container">
                                <div class="media-container-row">
                                    <div class="col-12 col-md-12">

                                        <div class="clearfix"></div>
                                        <div id="bootstrap-accordion_9" class="panel-group accordionStyles accordion"
                                             role="tablist" aria-multiselectable="true">
                                            @foreach($licenses as $license)
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingOne">
                                                        <a role="button" class="panel-title collapsed text-black"
                                                           data-toggle="collapse" data-core=""
                                                           href="#collapse{{ $license->id }}_9" aria-expanded="false"
                                                           aria-controls="collapse1">
                                                            <h4 class="mbr-fonts-style display-5">
                                                                <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>
                                                                {{ $license->names }}'s {{ $license->category->name }}
                                                            </h4>
                                                        </a>
                                                    </div>
                                                    <div id="collapse{{ $license->id }}_9"
                                                         class="panel-collapse noScroll collapse " role="tabpanel"
                                                         aria-labelledby="headingOne"
                                                         data-parent="#bootstrap-accordion_9">
                                                        <div class="panel-body p-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group col-md-12">
                                                                        <label>Number Of Images</label>
                                                                        <select class="form-control number"
                                                                                lic_id="{{ $license->id }}">
                                                                            <optgroup label="Number Of Images">
                                                                                @for($i=1;$i<=10;$i++)
                                                                                    <option value="{{ $i }}">
                                                                                        {{ $i }}
                                                                                    </option>
                                                                                @endfor
                                                                            </optgroup>
                                                                        </select>
                                                                    </div>
                                                                    <form method="post"
                                                                          action="{{ url("license/images/".$license->id) }}"
                                                                          class="form-horizontal"
                                                                          enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="main"
                                                                             id="main{{ $license->id }}"></div>
                                                                        @include("layouts.error")
                                                                        <span class="input-group-btn"><button
                                                                                    type="submit"
                                                                                    class="btn btn-primary btn-form display-4 rounded">UPLOAD IMAGES</button></span>
                                                                        <input type="hidden" id="out{{ $license->id }}"
                                                                               name="number">
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
<script src="{{ asset('assets/web/assets/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
    $(function () {

        $(".number").change(function () {
            var html = "";
            var selected = $(this).val();
            var id = $(this).attr("lic_id");
            for (var i = 1; i <= selected; i++) {
                html += "<input type='file' required accept='image/*' name='image" + i + "'>";
                $("#out" + id).val(i);
            }
            $("#main" + id).html(html);
        });

    })

</script>