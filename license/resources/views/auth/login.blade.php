@extends('layouts.master')
@section("title","Login Form")
@section("head","Login Form")
@section("breadcrumb")

    <li class="active">Login</li>
@endsection
@section('contents')

    <section class="engine"><a href="{{ url("/") }}">Missed License</a></section><section class="mbr-section form1 cid-r4Q3zbPjDq" id="form1-j">

        <div class="container">
            <div class="row justify-content-center">
                <div class="title col-12 col-lg-6">
                    <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
                        LOGIN FORM
                    </h2>
                    <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">
                        PROVIDE CORRECTLY YOUR REGISTERED PHONE NUMBER AND PASSWORD&nbsp;</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="media-container-column col-lg-6" data-form-type="formoid">

                    <form class="mbr-form" action="{{ route('login') }}" method="post" data-form-title="Login Form"><input type="hidden" name="email" data-form-email="true" value="LIdGbWhTBNbcqoaRvvcXLsYHAWQaMt57SA4aaZ76Brd2DqOS3PL3tizE1lXmCaPfT9916RRtQh8meEBBhTFRcXG5bJ7vNjTKDiGsrOegNPK7AMt1VSzG+A7/JBRMKs1/">
                     {{ csrf_field() }}
                        <div class="row row-sm-offset">
                            <div class="col-md-12 multi-horizontal" data-for="phone">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="phone-form1-j">Phone Number</label>
                                    <input type="tel" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" data-form-field="Phone" id="phone-form1-j">
                                </div>
                            </div>

                            <div class="col-md-12 multi-horizontal" data-for="password">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="password-form1-j">Password</label>
                                    <input type="password" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="password" data-form-field="Password" required="" id="password-form1-j">
                                </div>
                            </div>
                        </div>
                        @include("layouts.error")
                        <span class="input-group-btn"><button href="" type="submit" class="btn btn-primary btn-form display-4">LOGIN</button></span>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

