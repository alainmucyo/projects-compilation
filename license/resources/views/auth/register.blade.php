@extends('layouts.master')
@section("title","Register Form")
@section("head","Register")
@section("breadcrumb")

    <li class="active">Register</li>
@endsection
@section('contents')

    <section class="mbr-section form1 cid-r4Q5rzz6jf" id="form1-l">

        <div class="container">
            <div class="row justify-content-center">
                <div class="title col-12 col-lg-8">
                    <div class="card">
                        <div class="card-header">
                    <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
                        REGISTER FORM
                    </h2>
                    <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">
                        Easily fill this form with your truly required credentials and please don't forget.
                    </h3>
                        </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="media-container-column col-lg-8" data-form-type="formoid">
                    <div class="card">
                        <div class="card-body"></div>
                    <div data-form-alert="" hidden="">
                        Thanks for filling out the form!
                    </div>

                    <form class="mbr-form" action="{{ route('register') }}" method="post"
                          data-form-title="Register Form"><input type="hidden" name="email" data-form-email="true"
                                                                 value="lPXK4GGDsxspp6KXsO6ZqyPji9sXbrXWSLeacCtUSgZCCMe7u3+FmCCnYqvRM0lSG/d6xPOjZiju3O2rF+ng5VJloNS7NRbjlvSn8phWZ22C+CW4fU/6454saAtzK3+J">
                        {{ csrf_field() }}
                        <div class="row row-sm-offset">
                            <div class="col-md-6 multi-horizontal" data-for="name">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="name-form1-l">Full
                                        Names</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" data-form-field="Name" required="" value="{{ old("name") }}" id="name-form1-l">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('name') }}</strong></small>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 multi-horizontal" data-for="phone">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="phone-form1-l">Phone
                                        Number(MTN)</label>
                                    <input type="number"
                                           class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                           data-form-field="Phone Number" required min="100000000" minlength="10"
                                           max="1000000000"
                                           name="phone" placeholder="* 0789999999" id="phone-form1-l" value="{{ old("phone") }}">
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('phone') }}</strong></small>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row row-sm-offset">
                            <div class="col-md-4 multi-horizontal" data-for="province">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="province-form1-l">Province</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('province') ? ' is-invalid' : '' }}"
                                           name="province" data-form-field="Province" required="" id="province-form1-l"  value="{{ old("province") }}">
                                    @if ($errors->has('province'))
                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('province') }}</strong></small>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 multi-horizontal" data-for="district">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="district-form1-l">District</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('district') ? ' is-invalid' : '' }}"
                                           name="district" data-form-field="District" required="" id="district-form1-l"  value="{{ old("district") }}">
                                    @if ($errors->has('district'))
                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('district') }}</strong></small>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 multi-horizontal" data-for="sector">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="sector-form1-l">Sector</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('sector') ? ' is-invalid' : '' }}"
                                           name="sector" data-form-field="Phone" id="sector-form1-l"  value="{{ old("sector") }}">
                                    @if ($errors->has('sector'))
                                        <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('sector') }}</strong></small>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group" data-for="password">
                            <label class="form-control-label mbr-fonts-style display-7"
                                   for="password-form1-l">Password</label>
                            <input type="password"
                                   class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password" data-form-field="Password" required="" id="password-form1-l"  value="{{ old("password") }}">
                        </div>
                        <div class="form-group" data-for="password">
                            <label class="form-control-label mbr-fonts-style display-7" for="password_conf-form1-l">Password
                                Confirmation</label>
                            <input type="password"
                                   class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password_confirmation" data-form-field="Password" required=""
                                   id="password_conf-form1-l">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                  <small class="text-danger"> <strong>{{ $errors->first('password') }}</strong></small>
                                    </span>
                            @endif
                        </div>

                        <span class="input-group-btn"><button href="" type="submit"
                                                              class="btn btn-primary btn-form display-4">REGISTER</button></span>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>

    <section class="engine"><a href="{{ url("/") }}">online missed license</a></section>
@endsection
