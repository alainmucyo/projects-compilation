@if(count($errors))
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <li class="text-error"><b>{{ $error }}</b></li>
        @endforeach
    </div>
@endif