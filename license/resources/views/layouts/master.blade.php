<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.8.3, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="{{ asset("assets/images/apple-touch-icon-144-precomposed-122x122.png") }}"
          type="image/x-icon">
    <meta name="description" content="">
    <title>@yield("title")</title>
    <link rel="stylesheet" href="{{ asset('assets/web/assets/mobirise-icons/mobirise-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tether/tether.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-reboot.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/socicon/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/animatecss/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dropdown/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/gallery/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/mobirise/css/mbr-additional.css') }}" type="text/css">


</head>
<body>
<section class="menu cid-r4PMk2HKeE" once="menu" id="menu2-c">

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="{{ url("/") }}">
                        <img src="{{ url("assets/images/apple-touch-icon-144-precomposed-122x122.png") }}" alt="LOGO"
                             title=""
                             style="height: 3.8rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap">
                    <a class="navbar-caption text-black display-4" href="https://mobirise.com">
                        FUCK
                    </a>
                </span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="{{ url("/") }}">
                        <span
                                class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Home
                    </a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="{{ url("/register") }}">
                        <span
                                class="mbri-users mbr-iconfont mbr-iconfont-btn"></span>
                        Register
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="{{ url("/login") }}">
                        <span
                                class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>
                        Login
                    </a>
                </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link link text-black display-4" href="{{ url("/home") }}">
                        <span
                                class="mbri-browse mbr-iconfont mbr-iconfont-btn"></span>
                            Licenses
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link text-black display-4" href="{{ url("/add_license") }}">
                        <span
                                class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>
                            Add License
                        </a>
                    </li>
                    <li class="nav-item dropdown open">
                        <a class="nav-link link text-black dropdown-toggle display-4" href="https://mobirise.com"
                           data-toggle="dropdown-submenu"><span
                                    class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>
                            {{ auth()->user()->name }}
                        </a>
                        <div class="dropdown-menu"><a class="text-black dropdown-item display-4"
                                                      href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><span
                                        class="mbri-logout mbr-iconfont mbr-iconfont-btn">Logout</span></a></div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </li>
                    @endguest
            </ul>
            <div class="navbar-buttons mbr-section-btn">
                <a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    078888888
                </a>
            </div>
        </div>
    </nav>
</section>

@yield("contents")
<section class="cid-r4QcZnPFj1" id="footer1-n">
    <div class="mbr-overlay" style="background-color: rgb(60, 60, 60); opacity: 0.5;"></div>

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="{{ url("/") }}">
                        <img src="{{ asset("assets/images/apple-touch-icon-144-precomposed-144x144.png") }}"
                             alt="Mobirise" title="">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Address
                </h5>
                <p class="mbr-text">
                    1234 Street Name
                    <br>City, AA 99999
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Contacts
                </h5>
                <p class="mbr-text">
                    Email: support@mobirise.com
                    <br>Phone: +1 (0) 000 0000 001
                    <br>Fax: +1 (0) 000 0000 002
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Links
                </h5>
                <p class="mbr-text">
                    <a class="text-primary" href="https://mobirise.com/">Website builder</a>
                    <br><a class="text-primary" href="https://mobirise.com/mobirise-free-win.zip">Download for
                        Windows</a>
                    <br><a class="text-primary" href="https://mobirise.com/mobirise-free-mac.zip">Download for Mac</a>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2017 Mobirise - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://twitter.com/mobirise" target="_blank">
                                <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.youtube.com/c/mobirise" target="_blank">
                                <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://instagram.com/mobirise" target="_blank">
                                <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://plus.google.com/u/0/+Mobirise" target="_blank">
                                <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.behance.net/Mobirise" target="_blank">
                                <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="{{ asset('assets/web/assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/popper/popper.min.js') }}"></script>
<script src="{{ asset('assets/tether/tether.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/smoothscroll/smooth-scroll.js') }}"></script>
<script src="{{ asset('assets/ytplayer/jquery.mb.ytplayer.min.js') }}"></script>
<script src="{{ asset('assets/vimeoplayer/jquery.mb.vimeo_player.js') }}"></script>
<script src="{{ asset("assets/masonry/masonry.pkgd.min.js") }}"></script>
<script src="{{ asset("assets/imagesloaded/imagesloaded.pkgd.min.js") }}"></script>
<script src="{{ asset('assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js') }}"></script>
<script src="{{ asset('assets/viewportchecker/jquery.viewportchecker.js') }}"></script>
<script src="{{ asset('assets/dropdown/js/script.min.js') }}"></script>
<script src="{{ asset('assets/touchswipe/jquery.touch-swipe.min.js') }}"></script>
<script src="{{ asset('assets/theme/js/script.js') }}"></script>
<script src="{{ asset('assets/gallery/player.min.js') }}"></script>
<script src="{{ asset('assets/gallery/script.js') }}"></script>
<script src="{{ asset('assets/slidervideo/script.js') }}"></script>
<script src="{{ asset("jquery.noty.packaged.js") }}"></script>
<script type="text/javascript">
    $(function () {
        $("#my-carousel-item").addClass("active");
        $("#my-carousel").addClass("active");
    })
</script>
@if(session('success'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: '{{ session('success') }}!',
            layout: 'topCenter',
            type: 'success'
        });
    </script>
@endif
<div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i></i></a></div>
<input name="animation" type="hidden">
</body>
</html>