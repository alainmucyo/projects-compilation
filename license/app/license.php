<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class license extends Model

{
    protected $guarded=[];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function images(){
        return $this->hasMany(Image::class);
    }
}
