<?php

namespace App\Http\Controllers;

use App\Category;
use App\license;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $licenses = license::paginate(6);
        $categories = Category::get();
        $selected=0;
        return view('home', compact("licenses", "categories","selected"));
    }
}
