<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use App\license;
use App\User;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {
        $categories=Category::get();
        $licenses=auth()->user()->licenses;
        return view("license.add_license",compact("categories","licenses"));
    }

    public function store()
    {
        $this->validate(\request(), ['image' => 'required|image', 'phone' => 'required|numeric|max:0789999999|min:0780000000|unique:licenses']);
        if (\request()->hasFile("image")) {
            license::create([
                "names" => \request("names"),
                "category_id" => \request("category"),
                "address" => \request("address"),
                "phone" => \request("phone"),
                "user_id" => auth()->user()->id,
                "more" => \request("more"),
                "image" => \request("image")->store("public/licenses")
            ]);
        }
        session()->flash("success", "License Added Successfully, Wait on your mobile money");
        return back();
    }

    public function licenses($search)
    {
        $search = trim(htmlspecialchars($search));
        $licenses = license::where("names", "LIKE", "%" . $search . "%")->paginate(6);
        return view("license.licenses", compact("licenses", "search"));

    }

    public function this_license($id)
    {
        $license = license::find($id);
        return view("license.this_license", compact("license"));
    }
    public function storeImages($id){

        $number=\request("number");
        for ($i=1;$i<=$number;$i++){
          //  echo "$i hello";
            $this->validate(\request(),["image".$i=>"required|image"]);
            Image::create([
                "image"=>\request("image".$i)->store("public/licenses"),
                "license_id"=>$id
            ]);
        }
        session()->flash("success","$number images uploaded successfully");
        return back();
    }
    public function gallery($id){
        $images=license::find($id)->images;
        $i=0;
        $b=0;
        $c=0;
        return view("license.gallery",compact("images","i","b","c"));
    }
    public function sort($selected){
        $cat=Category::where("name",$selected)->first()->id;
        $licenses=license::where("status",0)->where("category_id",$cat)->paginate(6);
        $categories=Category::get();
        return view("home",compact("licenses","categories","selected"));
    }

}
