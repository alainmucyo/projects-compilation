<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded=[];
    public function licenses(){
        return $this->hasMany(license::class);
    }

}
