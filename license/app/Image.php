<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded=[];
    public function license(){
        return $this->belongsTo(license::class);
    }
}
