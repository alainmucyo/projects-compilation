<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("/add_license","LicenseController@index");
Route::post("/add_license","LicenseController@store");
Route::get("/licenses","LicenseController@licenses");
Route::get("/this_license/{id}","LicenseController@this_license");
Route::get("/license/find/{search}","LicenseController@licenses");
Route::get("/license/find/","HomeController@index");
Route::post("/license/images/{id}","LicenseController@storeImages");
Route::get("/license/gallery/{id}","LicenseController@gallery");
Route::get("/license/cat/{selected}","LicenseController@sort");