<?php

Route::get('/', function () {
    return redirect("/login");
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::apiResource("/api/product", "ProductController");
Route::apiResource("/api/size", "SizeController");
Route::get("/products", "HomeController@products");
Route::get("/addedList", "AddedController@addedList");
Route::get("/added", "AddedController@getAdded");
Route::post("/added", "AddedController@postAdded");
Route::delete("/remove/{index}", "AddedController@remove");
Route::post("/api/details", "MovementController@createDetails");
Route::get("/reports", "ReportsController@index");
Route::get("/requisitions", "RequisitionController@requisitions");
Route::put("/requisitions/update/{index}", "RequisitionController@updateSession");
Route::apiResource("/requisition", "RequisitionController");
Route::get("/asked-requisitions", "RequisitionController@asked");
Route::get("/requested-requisitions", "RequisitionController@requested");
Route::get("/receipt/{receipt}", "RequisitionController@receipt");
Route::post("/approve/{receipt}", "RequisitionController@approve");
Route::get("/dashboard", "HomeController@dashboard");
Route::prefix('production')->group(function () {
    Route::get("/", 'ProductionController@index');
    Route::post("/", 'ProductionController@store');
    Route::put("/{production}", 'ProductionController@update');
    Route::post("/machine", "ProductionController@storeMachine");
    Route::post("/maintenance", "ProductionController@storeMaintenance");
    Route::get("/report", 'ProductionController@report');
    Route::get("/report/yearly", 'ProductionController@yearly');
});
Route::prefix('sale')->group(function () {
    Route::get("/", 'SalesController@index');
    Route::post("/client", 'SalesController@storeClient');
    Route::get("/selling", 'SalesController@selling');
    Route::get("/clients", 'SalesController@clients');
    Route::get("/products", "SalesController@products");
    Route::get("/random", "SalesController@random");
    Route::post("/selling", "SalesController@sell");
    Route::put("/payment", "SalesController@payment");
    Route::get("/report", "SalesController@report");
    Route::get("/report/{client}", "SalesController@clientSingleReport");
    Route::get("/sale/{receipt}", "SalesController@sales");
    Route::get("/product/report", 'SalesController@productReport');
    Route::post("/reverse", "SalesController@reverse");
    Route::post("/reverse", "SalesController@reverse");
});
Route::prefix('admin')->group(function () {
    Route::get("/", "HomeController@dashboard");
    Route::get("/maintenance", "AdminController@maintenance");
    Route::get("/unit", "AdminController@unit");
    Route::get("/operators", "AdminController@operator");
});

Route::apiResource("/api/user", "UsersController");
Route::get("/users", "UsersController@users");
Route::post("/profile/{user}", "UsersController@profile");
Route::get("/profile", "UsersController@getProfile");
Route::get("/user", function () {
    return response(["user" => auth()->user()]);
});
Route::prefix('stock')->group(function () {
    ROute::get("/", 'StockController@index');
    Route::get("/report/yearly", 'StockController@year');
    Route::get("/report", "StockController@report");
});
Route::post("/client", "ClientController@store");
Route::get("/client/delete/{client}", "ClientController@destroy");
Route::apiResource("/api/unit", "SizeController");
Route::apiResource("/api/operator", "OperatorController");