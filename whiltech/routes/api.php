<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get("/operators", function () {
    return \App\Operator::where("status", 1)->get();
});
Route::get("/machines", function () {
    return \App\Machine::where("status", 1)->get();
});
Route::get("/products", function () {
    return \App\Product::where("category", "Product")->where("status", 1)->get();
});
Route::get("/raw_products", function () {
    return \App\Product::where("category", "Raw Material")->where("status", 1)->get();
});