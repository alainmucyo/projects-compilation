-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2019 at 08:41 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `company`, `email`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dr Alain', 'UMUCYO Web and app devs', NULL, '078888888', 1, '2019-06-12 12:36:22', '2019-06-12 12:36:22');

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_date` date NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`id`, `name`, `model`, `serial`, `purchase_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Tinga Tinga', 'OG', '0187836', '2019-06-10', 1, '2019-06-11 17:21:51', '2019-06-11 17:21:51'),
(2, 'Test', 'testing', '00000', '2019-06-08', 1, '2019-06-11 17:22:38', '2019-06-11 17:22:38');

-- --------------------------------------------------------

--
-- Table structure for table `maintenances`
--

CREATE TABLE `maintenances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `issue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reporter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `machine_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maintenances`
--

INSERT INTO `maintenances` (`id`, `issue`, `reporter`, `machine_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'This shit is complicated to use.', 'Dr Alan', 2, 1, '2019-06-12 06:06:08', '2019-06-12 06:06:08'),
(2, 'Please Replace this fuckin\' machine.', 'Dr Alan', 2, 1, '2019-06-12 06:19:43', '2019-06-12 06:19:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_06_07_185132_create_products_table', 1),
(2, '2019_06_07_185259_create_sizes_table', 1),
(4, '2019_06_09_120119_create_product_details_table', 2),
(5, '2019_06_09_114358_create_movements_table', 3),
(8, '2019_06_10_132900_create_requisitions_table', 4),
(9, '2019_06_10_174050_create_receipts_table', 4),
(10, '2019_06_11_183346_create_machines_table', 5),
(11, '2019_06_11_183427_create_productions_table', 5),
(12, '2019_06_11_183735_create_operators_table', 5),
(13, '2019_06_12_075037_create_maintenances_table', 6),
(14, '2019_06_12_140500_create_clients_table', 7),
(15, '2019_06_13_045747_create_sales_table', 8),
(16, '2019_06_13_045854_create_sale_receipts_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `movements`
--

CREATE TABLE `movements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `opening` int(11) NOT NULL DEFAULT '0',
  `closing` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movements`
--

INSERT INTO `movements` (`id`, `opening`, `closing`, `product_id`, `created_at`, `updated_at`) VALUES
(2, 0, 40, 11, '2019-05-08 10:36:31', '2019-06-09 11:39:10'),
(3, 40, 104, 11, '2019-06-09 11:41:39', '2019-06-09 16:48:38'),
(4, 0, 42, 12, '2019-06-09 14:37:31', '2019-06-09 16:48:38'),
(10, 104, 3579, 11, '2019-06-10 03:36:49', '2019-06-10 07:02:18'),
(11, 42, 50, 12, '2019-06-10 03:36:49', '2019-06-10 07:02:19'),
(12, 50, 7, 12, '2019-06-11 07:37:21', '2019-06-11 07:37:21'),
(15, 3579, 3552, 11, '2019-06-11 10:11:57', '2019-06-11 13:58:06'),
(16, 0, 0, 13, '2019-06-11 16:11:45', '2019-06-11 16:11:45'),
(17, 0, 0, 14, '2019-06-11 16:12:10', '2019-06-11 16:12:10'),
(18, 0, 13, 14, '2019-06-12 06:35:24', '2019-06-12 10:06:05'),
(19, 0, 2, 13, '2019-06-12 09:01:37', '2019-06-12 10:06:05'),
(20, 3552, 3545, 11, '2019-06-12 09:01:37', '2019-06-12 09:20:25'),
(21, 3545, 3565, 11, '2019-06-15 08:28:30', '2019-06-15 08:28:30'),
(22, 13, 14, 14, '2019-06-15 08:31:52', '2019-06-15 08:31:52'),
(23, 2, 99, 13, '2019-06-15 08:51:06', '2019-06-15 08:52:06'),
(24, 3565, 3561, 11, '2019-06-17 06:13:19', '2019-06-17 06:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `operators`
--

CREATE TABLE `operators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `operators`
--

INSERT INTO `operators` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alain', 1, '2019-06-11 16:59:10', '2019-06-11 16:59:10'),
(2, 'Mucyo', 1, '2019-06-11 16:59:15', '2019-06-11 16:59:15'),
(3, 'Mr Alan', 1, '2019-06-11 16:59:20', '2019-06-16 09:09:10'),
(4, 'Eng. Mucyo', 1, '2019-06-11 16:59:42', '2019-06-11 16:59:42'),
(5, 'New', 0, '2019-06-16 09:07:35', '2019-06-16 09:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `productions`
--

CREATE TABLE `productions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `machine_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `shift` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expected` int(11) NOT NULL,
  `actual` int(11) NOT NULL,
  `wasted` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `product_detail_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productions`
--

INSERT INTO `productions` (`id`, `machine_id`, `product_id`, `operator_id`, `shift`, `expected`, `actual`, `wasted`, `comment`, `product_detail_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 11, 1, 'Day', 4, 4, 0, 'Done pretty good', 0, 1, '2019-06-12 05:49:03', '2019-06-14 03:23:24'),
(2, 2, 14, 4, 'Night', 21, 5, 3, 'I was drunk.', 0, 1, '2019-06-14 22:00:00', '2019-06-14 11:47:03'),
(3, 1, 14, 4, 'Day', 20, 20, 0, 'Okay.', 0, 1, '2019-02-12 06:35:24', '2019-06-15 08:31:52'),
(4, 1, 14, 4, 'Night', 21, 16, 6, 'Soorry', 0, 1, '2019-06-12 09:14:57', '2019-06-15 03:08:35'),
(5, 1, 11, 1, 'Day', 20, 20, 0, NULL, 44, 1, '2019-06-15 08:28:30', '2019-06-15 08:28:30'),
(6, 1, 13, 1, 'Day', 100, 97, 3, NULL, 46, 1, '2019-06-15 08:51:06', '2019-06-15 08:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size_id` int(11) NOT NULL,
  `sale_stock` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category`, `supplier`, `description`, `tag`, `size_id`, `sale_stock`, `status`, `created_at`, `updated_at`) VALUES
(11, 'Test', 'Product', 'test', 'This is test', 'test', 2, 5, 1, '2019-06-09 10:36:30', '2019-06-17 06:13:19'),
(12, 'new', 'Raw Material', 'new', 'This is product', 'new', 2, 0, 1, '2019-06-09 14:37:31', '2019-06-13 05:11:14'),
(13, 'Papers', 'Product', 'Dunder Miflin', 'This is a paper.', 'paper', 2, 346, 1, '2019-06-11 16:11:45', '2019-06-16 12:13:34'),
(14, 'Hello', 'Product', 'test', 'This is hello', 'hello', 1, 1, 1, '2019-06-11 16:12:10', '2019-06-16 12:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `received` int(11) NOT NULL DEFAULT '0',
  `dispatched` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`id`, `received`, `dispatched`, `product_id`, `created_at`, `updated_at`) VALUES
(3, 0, 10, 11, '2019-06-09 12:06:21', '2019-06-09 12:06:21'),
(4, 0, 10, 11, '2019-06-09 12:07:22', '2019-06-09 12:07:22'),
(5, 0, 10, 11, '2019-06-09 12:08:01', '2019-06-09 12:08:01'),
(6, 3, 1, 11, '2019-06-09 16:37:08', '2019-06-09 16:37:08'),
(7, 3, 1, 11, '2019-06-09 16:45:58', '2019-06-09 16:45:58'),
(8, 9, 2, 12, '2019-06-09 16:45:58', '2019-06-09 16:45:58'),
(9, 6, 3, 11, '2019-06-09 16:48:38', '2019-06-09 16:48:38'),
(10, 4, 2, 12, '2019-06-09 16:48:38', '2019-06-09 16:48:38'),
(15, 5, 2, 11, '2019-06-10 03:36:49', '2019-06-10 03:36:49'),
(16, 12, 7, 12, '2019-06-10 03:36:49', '2019-06-10 03:36:49'),
(17, 20, 0, 11, '2019-06-10 03:37:25', '2019-06-10 03:37:25'),
(18, 3445, 0, 11, '2019-06-10 06:54:50', '2019-06-10 06:54:50'),
(19, 7, 0, 11, '2019-06-10 07:02:18', '2019-06-10 07:02:18'),
(20, 3, 0, 12, '2019-06-10 07:02:19', '2019-06-10 07:02:19'),
(21, 0, 43, 12, '2019-06-11 07:37:21', '2019-06-11 07:37:21'),
(25, 0, 4, 11, '2019-06-11 10:11:58', '2019-06-11 10:11:58'),
(26, 0, 2, 11, '2019-06-11 10:15:14', '2019-06-11 10:15:14'),
(27, 0, 6, 11, '2019-06-11 11:48:51', '2019-06-11 11:48:51'),
(28, 0, 5, 11, '2019-06-11 11:58:58', '2019-06-11 11:58:58'),
(29, 0, 10, 11, '2019-06-11 13:58:06', '2019-06-11 13:58:06'),
(30, 19, 0, 14, '2019-06-12 06:35:24', '2019-06-12 06:35:24'),
(31, 0, 2, 14, '2019-06-12 09:01:37', '2019-06-12 09:01:37'),
(32, 0, 3, 13, '2019-06-12 09:01:37', '2019-06-12 09:01:37'),
(33, 0, 4, 11, '2019-06-12 09:01:37', '2019-06-12 09:01:37'),
(34, 20, 0, 13, '2019-06-12 09:06:45', '2019-06-12 09:06:45'),
(35, 15, 0, 14, '2019-06-12 09:14:57', '2019-06-12 09:14:57'),
(36, 0, 3, 11, '2019-06-12 09:20:25', '2019-06-12 09:20:25'),
(37, 0, 6, 13, '2019-06-12 09:20:25', '2019-06-12 09:20:25'),
(38, 0, 7, 13, '2019-06-12 09:59:21', '2019-06-12 09:59:21'),
(39, 0, 10, 14, '2019-06-12 09:59:21', '2019-06-12 09:59:21'),
(40, 0, 3, 14, '2019-06-12 10:02:03', '2019-06-12 10:02:03'),
(41, 0, 3, 13, '2019-06-12 10:02:03', '2019-06-12 10:02:03'),
(42, 0, 2, 13, '2019-06-12 10:06:05', '2019-06-12 10:06:05'),
(43, 0, 6, 14, '2019-06-12 10:06:05', '2019-06-12 10:06:05'),
(44, 20, 0, 11, '2019-06-15 08:28:30', '2019-06-15 08:28:30'),
(45, 20, 0, 14, '2019-06-15 08:33:27', '2019-06-15 08:33:27'),
(46, 97, 0, 13, '2019-06-15 08:51:06', '2019-06-15 08:52:06'),
(47, 0, 4, 11, '2019-06-17 06:13:19', '2019-06-17 06:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `receipts`
--

INSERT INTO `receipts` (`id`, `number`, `approved`, `user_id`, `created_at`, `updated_at`) VALUES
(10, '201906114685', 1, 1, '2019-06-11 06:49:58', '2019-06-11 10:11:58'),
(11, '201906119570', 1, 1, '2019-06-11 10:14:06', '2019-06-11 10:15:14'),
(12, '201906116647', 1, 1, '2019-06-11 11:45:47', '2019-06-11 11:48:51'),
(13, '201906112030', 1, 1, '2019-06-11 11:55:53', '2019-06-11 11:58:58'),
(14, '201906119740', 1, 1, '2019-06-11 13:56:52', '2019-06-11 13:58:06'),
(15, '201906126454', 1, 1, '2019-06-12 08:55:43', '2019-06-12 09:01:37'),
(16, '201906125231', 1, 1, '2019-06-12 09:19:30', '2019-06-12 09:20:25'),
(17, '201906124438', 1, 1, '2019-06-12 09:57:47', '2019-06-12 09:59:21'),
(18, '201906121391', 1, 1, '2019-06-12 10:01:08', '2019-06-12 10:02:03'),
(19, '201906129832', 1, 1, '2019-06-12 10:05:34', '2019-06-12 10:06:06'),
(20, '201906172405', 1, 3, '2019-06-17 06:10:20', '2019-06-17 06:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `requisitions`
--

CREATE TABLE `requisitions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `product_id` int(11) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requisitions`
--

INSERT INTO `requisitions` (`id`, `quantity`, `details`, `product_id`, `receipt_id`, `user_id`, `product_name`, `product_tag`, `product_size`, `product_description`, `approved`, `created_at`, `updated_at`) VALUES
(7, 4, NULL, 11, 10, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-11 06:49:58', '2019-06-11 10:11:58'),
(8, 2, NULL, 11, 11, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-11 10:14:06', '2019-06-11 10:15:14'),
(9, 6, NULL, 11, 12, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-11 11:45:47', '2019-06-11 11:48:51'),
(10, 5, NULL, 11, 13, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-11 11:55:53', '2019-06-11 11:58:58'),
(11, 10, NULL, 11, 14, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-11 13:56:52', '2019-06-11 13:58:06'),
(12, 2, NULL, 14, 15, 1, 'Hello', 'hello', 'Test', 'This is hello', 1, '2019-06-12 08:55:43', '2019-06-12 09:01:37'),
(13, 3, NULL, 13, 15, 1, 'Papers', 'paper', 'New', 'This is a paper.', 1, '2019-06-12 08:55:43', '2019-06-12 09:01:37'),
(14, 4, NULL, 11, 15, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-12 08:55:43', '2019-06-12 09:01:37'),
(15, 3, NULL, 11, 16, 1, 'Test', 'test', 'New', 'This is test', 1, '2019-06-12 09:19:30', '2019-06-12 09:20:25'),
(16, 6, NULL, 13, 16, 1, 'Papers', 'paper', 'New', 'This is a paper.', 1, '2019-06-12 09:19:30', '2019-06-12 09:20:25'),
(17, 7, NULL, 13, 17, 1, 'Papers', 'paper', 'New', 'This is a paper.', 1, '2019-06-12 09:57:47', '2019-06-12 09:59:21'),
(18, 10, NULL, 14, 17, 1, 'Hello', 'hello', 'Test', 'This is hello', 1, '2019-06-12 09:57:47', '2019-06-12 09:59:21'),
(19, 3, NULL, 14, 18, 1, 'Hello', 'hello', 'Test', 'This is hello', 1, '2019-06-12 10:01:08', '2019-06-12 10:02:03'),
(20, 3, NULL, 13, 18, 1, 'Papers', 'paper', 'New', 'This is a paper.', 1, '2019-06-12 10:01:09', '2019-06-12 10:02:03'),
(21, 2, NULL, 13, 19, 1, 'Papers', 'paper', 'New', 'This is a paper.', 1, '2019-06-12 10:05:34', '2019-06-12 10:06:06'),
(22, 6, NULL, 14, 19, 1, 'Hello', 'hello', 'Test', 'This is hello', 1, '2019-06-12 10:05:34', '2019-06-12 10:06:06'),
(23, 4, NULL, 11, 20, 3, 'Test', 'test', 'New', 'This is test', 1, '2019-06-17 06:10:20', '2019-06-17 06:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sale_receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `unit_price` bigint(20) NOT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sale_receipt_id`, `product_id`, `client_id`, `unit_price`, `payment`, `quantity`, `created_at`, `updated_at`) VALUES
(3, 3, 12, 1, 1000, 'Bank', 500, '2019-06-13 05:11:14', '2019-06-13 05:11:14'),
(4, 3, 13, 1, 5000, 'Bank', 10, '2019-06-13 05:11:15', '2019-06-13 05:11:15'),
(5, 4, 13, 1, 1000, 'Cheque', 40, '2019-06-13 06:01:29', '2019-06-13 06:01:29'),
(6, 4, 14, 1, 1000, 'Cheque', 2, '2019-06-13 06:01:29', '2019-06-13 06:01:29'),
(7, 5, 13, 1, 100, 'Credit', 200, '2019-06-13 11:37:36', '2019-06-13 11:37:36'),
(8, 5, 14, 1, 20, 'Credit', 2, '2019-06-13 11:37:36', '2019-06-13 11:37:36'),
(9, 6, 13, 1, 1000, 'Credit', 100, '2019-06-16 12:13:02', '2019-06-16 12:13:02'),
(10, 6, 14, 1, 10000, 'Credit', 2, '2019-06-16 12:13:02', '2019-06-16 12:13:02'),
(11, 6, 11, 1, 100000, 'Credit', 1, '2019-06-16 12:13:02', '2019-06-16 12:13:02'),
(12, 7, 13, 1, 1000, 'Credit', 100, '2019-06-16 12:13:34', '2019-06-16 12:13:34'),
(13, 7, 14, 1, 10000, 'Credit', 2, '2019-06-16 12:13:34', '2019-06-16 12:13:34'),
(14, 7, 11, 1, 100000, 'Credit', 1, '2019-06-16 12:13:34', '2019-06-16 12:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `sale_receipts`
--

CREATE TABLE `sale_receipts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required_price` bigint(20) NOT NULL,
  `paid_price` bigint(20) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sale_receipts`
--

INSERT INTO `sale_receipts` (`id`, `number`, `required_price`, `paid_price`, `client_id`, `payment`, `created_at`, `updated_at`) VALUES
(3, '201906137734', 550000, 6000, 1, 'Bank', '2019-06-13 05:11:14', '2019-06-13 09:07:53'),
(4, '201906136325', 42000, 0, 1, 'Cheque', '2019-06-13 06:01:28', '2019-06-13 06:01:28'),
(5, '201906139001', 20040, 1000, 1, 'Credit', '2019-06-12 11:37:36', '2019-06-13 11:40:54'),
(6, '201906162692', 220000, 0, 1, 'Credit', '2019-06-16 12:13:02', '2019-06-16 12:13:02'),
(7, '201906162692', 220000, 0, 1, 'Credit', '2019-06-16 12:13:34', '2019-06-16 12:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Test', 1, '2019-06-07 17:02:04', '2019-06-07 17:02:04'),
(2, 'New', 1, '2019-06-07 17:02:10', '2019-06-07 17:02:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `type`, `avatar`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alain MUCYO', 'alain', NULL, 2, 'avatar/1FcV0sD0g7m5vUyvacEUmk6yqVcwiuPlF5sxUCti.jpeg', '$2y$10$S8lwcYerNohtBTIgIbhage89yEq2kKzCnKYFBdTwvHtBTbxfvQOA.', NULL, 1, '2019-06-07 17:24:43', '2019-06-14 16:14:08'),
(2, 'Mr Alan', 'alan', NULL, 1, NULL, '$2y$10$SYMPQeO0I5aWA3ngilFnJeygoTu38VfmLX9Pqgx.uPY26qcFnTX76', NULL, 1, '2019-06-16 07:42:20', '2019-06-16 07:46:58'),
(3, 'Seller', 'seller', NULL, 0, NULL, '$2y$10$Z4y3wJXZb/mW1IiwXI5hHuEDtPFW5DabT7hc9dks3wxhuo1h6yexq', NULL, 1, '2019-06-16 07:47:19', '2019-06-16 07:47:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintenances`
--
ALTER TABLE `maintenances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movements`
--
ALTER TABLE `movements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productions`
--
ALTER TABLE `productions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requisitions`
--
ALTER TABLE `requisitions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_receipts`
--
ALTER TABLE `sale_receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `maintenances`
--
ALTER TABLE `maintenances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `movements`
--
ALTER TABLE `movements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `operators`
--
ALTER TABLE `operators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `productions`
--
ALTER TABLE `productions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `requisitions`
--
ALTER TABLE `requisitions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sale_receipts`
--
ALTER TABLE `sale_receipts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
