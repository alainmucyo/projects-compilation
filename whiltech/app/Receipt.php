<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $guarded = [];

    public function requisitions()
    {
        return $this->hasMany(Requisition::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
