<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $guarded=[];

    public function productions(){
        return $this->hasMany(Production::class);
    }
}
