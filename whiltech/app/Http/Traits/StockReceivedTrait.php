<?php

namespace App\Http\Traits;

use App\ProductDetail;
use App\Requisition;
use Carbon\Carbon;

trait StockReceivedTrait
{

    public $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    public function weekly()
    {
        $days = array();
        $test = [0, 1, 2, 3, 4, 5, 6];
        $userType = auth()->user()->type;
        if ($userType == 0) {
            foreach ($test as $i) {
                array_push($days,
                    Requisition::whereBetween("created_at", [Carbon::now()->startOfWeek(),
                        Carbon::now()->endOfWeek()])->whereRaw('WEEKDAY(requisitions.created_at)=' . $i)->sum("quantity"));
            }
        } else {
            foreach ($test as $i) {
                array_push($days,
                    ProductDetail::whereBetween("created_at", [Carbon::now()->startOfWeek(),
                        Carbon::now()->endOfWeek()])->whereRaw('WEEKDAY(product_details.created_at)=' . $i)->sum("received"));
            }
        }
        return $days;
    }

    public function monthly()
    {
        $months = array();
        $userType = auth()->user()->type;
        if ($userType == 0) {
            for ($i = 1; $i <= 12; $i++) {
                array_push($months, Requisition::whereMonth("created_at", $i)->sum("quantity"));
            }
        } else {
            for ($i = 1; $i <= 12; $i++) {
                array_push($months, ProductDetail::whereMonth("created_at", $i)->sum("received"));
            }
        }
        return $months;

    }

    public function dailyData()
    {
        $userType = auth()->user()->type;
        if ($userType == 0)
            return Requisition::whereBetween("created_at", [Carbon::now()->startOfDay(),
                Carbon::now()->endOfDay()])->sum("quantity");
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfDay(),
            Carbon::now()->endOfDay()])->sum("received");
    }

    public function weeklyData()
    {
        $userType = auth()->user()->type;
        if ($userType == 0)
        return Requisition::whereBetween("created_at", [Carbon::now()->startOfWeek(),
            Carbon::now()->endOfWeek()])->sum("quantity");
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfWeek(),
            Carbon::now()->endOfWeek()])->sum("received");
    }

    public function monthlyData()
    {
        $userType = auth()->user()->type;
        if ($userType == 0)
            return Requisition::whereBetween("created_at", [Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()])->sum("quantity");

        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfMonth(),
            Carbon::now()->endOfMonth()])->sum("received");
    }

    public function yearlyData()
    {
        $userType = auth()->user()->type;
        if ($userType == 0)
            return Requisition::whereBetween("created_at", [Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear()])->sum("quantity");
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfYear(),
            Carbon::now()->endOfYear()])->sum("received");
    }
}