<?php

namespace App\Http\Controllers;

use App\Movement;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $movements=array();
        $months = Movement::selectRaw("monthname(created_at) month")
            ->groupBy("month")
            ->orderByRaw("min(created_at) desc")
            ->get();
        foreach ($months as $month){
            array_push($movements,Movement::whereMonth("created_at",$month->month)->sum("closing"));
        }
return $movements;
    }
}
