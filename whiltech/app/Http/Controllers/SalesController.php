<?php

namespace App\Http\Controllers;

use App\Charts\LineChart;
use App\Client;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\InventoryMiddleware;
use App\Http\Traits\SalesTrait;
use App\Product;
use App\Receipt;
use App\Sale;
use App\SaleReceipt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class SalesController extends Controller
{
    use SalesTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware([InventoryMiddleware::class])->except("clients");
        $this->middleware([AdminMiddleware::class])->except("report", "clients", "index", "productReport");
    }

    public function index()
    {

        $weeklyChart = new LineChart($this->days, $this->weekly(), "Weekly Income. (Rwf)");
        $monthlyChart = new LineChart($this->months, $this->monthly(), "Monthly Income. (Rwf)");
        $yearlyData = $this->yearlyData();
        $monthlyData = $this->monthlyData();
        $weeklyData = $this->weeklyData();
        $dailyData = $this->dailyData();
        $clients = Client::where("status", 1)->get();
        $i = 1;
        return view("seller.index", compact('i', 'clients', 'weeklyChart', 'monthlyChart', 'weeklyData', 'monthlyData', 'dailyData', 'yearlyData'));
    }

    public function storeClient(Request $request)
    {
        $request->validate([
            "name" => "required"
        ]);
        Client::create($request->all());
        session()->flash("success", "Client Added Successfully!");
        return redirect()->back();

    }

    public function sell(Request $request)
    {

        foreach ($request['products'] as $product) {
            $oldP = Product::find($product['product_id']);
            $newQty = $oldP->sale_stock - $product['quantity'];
            if ($newQty < 0) {
                return response("Quantity error in " . $oldP->name . ", " . $oldP->tag . " - " . $oldP->description, Response::HTTP_BAD_REQUEST);
            }
            $oldP->update(['sale_stock' => $newQty]);
        }
        $receipt = SaleReceipt::create([
            "number" => $request['random'],
            "required_price" => $request['totalPrice'],
            "client_id" => $request['client_id'],
            "payment" => $request['payment'],
            "details" => $request['details']
        ]);
        foreach ($request['products'] as $product) {

            Sale::create([
                "sale_receipt_id" => $receipt->id,
                "product_id" => $product['product_id'],
                "client_id" => $request['client_id'],
                "payment" => $request['payment'],
                "quantity" => $product['quantity'],
                "unit_price" => $product['price']
            ]);
        }
        return "ok";
    }

    public function report()
    {
        $i = 1;
        if (\request("month")) {
            $month = \request('month');
            $dates = SaleReceipt::whereYear("created_at", now()->year)->whereMonth('created_at', Carbon::parse($month)->month)->orderByDesc("created_at")->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->toDateString();
                });
            return view("seller.daily_report", compact('i', 'dates', "month"));
        } else if (\request("date")) {
            $day = \request('date');
            $payments = SaleReceipt::whereDate("created_at", $day)->get()
                ->groupBy(function ($val) {
                    return $val->payment;
                });
            return view("seller.payment_report", compact('i', 'payments', 'day'));
        } else if (\request("payment")) {
            $payment = \request('payment');
            $receipts = SaleReceipt::where("payment", $payment)->whereDate("created_at", request("day"))->get();
            return view("seller.payment_single_report", compact('i', 'receipts', 'payment'));
        } else {
            $months = SaleReceipt::whereYear("created_at", now()->year)->orderByDesc("created_at")->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->monthName;
                });
            return view("seller.report", compact('i', 'months'));
        }

    }

    public function clientSingleReport(Client $client)
    {
        $i = 1;
        $receipts = SaleReceipt::where("client_id", $client->id)->orderByDesc("created_at")->get();
        return view("seller.client_single_report", compact('receipts', 'client', "i"));
    }


    public function selling()
    {
        return view('seller.selling');
    }

    public function clients()
    {
        return Client::where("status", 1)->get();
    }

    public function products()
    {
        return Product::with("size")->where("category", "=", "Product")->where("status", 1)->get();
    }

    public function random()
    {
        $year = now()->year;
        $moth = date("m");
        $day = date("d");
        $rand = mt_rand(1000, 9999);
        return $year . $moth . $day . $rand;
    }

    public function payment(Request $request)
    {
        $receipt = SaleReceipt::with("client")->where("number", $request['receipt']);
        if ($receipt->count() <= 0) {
            return \response("Wrong receipt number", 400);
        }
        $oldAmount = $receipt->first()->paid_price;
        $newAmount = $oldAmount + $request['amount'];
        $receipt->first()->update(["paid_price" => $newAmount]);
        return $receipt->first();
    }

    public function sales($receipt)
    {
        $sales = Sale::with("product")->where("sale_receipt_id", $receipt)->orderByDesc("created_at")->get();
        return $sales;
    }

    public function productReport()
    {
        if (request("type")) {
            $type = request("type");
            if ($type == "week") {

                $products = Sale::whereBetween("updated_at", [Carbon::now()->startOfWeek(),
                    Carbon::now()->endOfWeek()])->get();

                $time = "This Week";
            } else if ($type == "month") {
                $products = Sale::whereYear("created_at", now()->year)->whereMonth('created_at', now())->get();

                $time = now()->monthName;
            } else {
                $products = Sale::whereYear("created_at", now()->year)->get();

                $time = now()->year;
            }
        } else {
            $products = Sale::whereDate('created_at', now())->get();
            $time = now()->toDateString();
        }
        $products = $products->groupBy(function ($val) {
            return $val->product_id;
        });
        $products = $products->sortByDesc(function ($group, $key) {
            return Product::find($key)->sales->count();
        });
        $i = 1;

        return view("sales.products.report", compact('products', 'i', "time"));
    }

    public function reverse(Request $request)
    {
        $receipt = SaleReceipt::find($request['id']);
        $sales = $receipt->sales;
        foreach ($sales as $sale) {
            $newQty = $sale->product->sale_stock + $sale->quantity;
            $sale->product->update(['sale_stock' => $newQty]);
            $sale->delete();
        }
        $receipt->delete();
        return redirect()->back();
    }
}
