<?php

namespace App\Http\Controllers;

use App\Http\Resources\SizeResource;
use App\Product;
use App\Requisition;
use App\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return SizeResource::collection(Size::where("status", 1)->get());
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required"
        ]);

        return new SizeResource(Size::create($request->all()));
    }

    public function update(Request $request, $size)
    {
        $size = Size::find($size);
        $request->validate([
            "name" => "required"
        ]);
        Requisition::where("product_size", $size->name)->update(["product_size" => $request['name']]);
        $size->update($request->all());
        return new SizeResource($size);
    }

    public function destroy($unit)
    {
        $size = Size::find($unit);
        $size->update(["status"=>0]);
    }
}
