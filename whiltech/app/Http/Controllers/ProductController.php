<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\SellerMiddleware;
use App\Http\Resources\ProductResource;
use App\Movement;
use App\Product;
use App\ProductDetail;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', SellerMiddleware::class, AdminMiddleware::class]);
    }

    public function index()
    {
        return ProductResource::collection(Product::where("status", 1)->get());
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "size" => "required",
            "description" => "required",
            "supplier" => "required",
            "category" => "required",
            "tag" => "required"
        ]);
        $request['size_id'] = $request['size']['id'];
        unset($request['size']);
        $product = Product::create($request->all());
        Movement::create(["product_id" => $product->id]);
        return new ProductResource($product);
    }

    public function show(Product $product)
    {
        //
    }

    public function edit(Product $product)
    {
        //
    }

    public function update(Request $request, Product $product)
    {

        $request->validate([
            "name" => "required",
            "size" => "required",
            "description" => "required",
            "supplier" => "required",
            "category" => "required",
            "tag" => "required"
        ]);
        $request['size_id'] = $request['size']['id'];
        unset($request['size']);
        $product->update($request->all());
        return new ProductResource($product);
    }

    public function destroy(Product $product)
    {
        $product->update(["status" => 0]);
    }
}
