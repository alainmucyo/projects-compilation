<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AddedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAdded()
    {

        return session()->get("products");
    }

    public function postAdded(Request $request)
    {
        session()->push("products", $request->all());
        return $request->all();
    }

    public function addedList()
    {
        return view("product.added");
    }

    public function remove($index)
    {
        $products = session()->get("products");
        unset($products[$index]);
        session()->remove("products");
        foreach ($products as $product) {
            session()->push("products", $product);
        }
        return $index;
    }
}
