<?php

namespace App\Http\Controllers;

use App\Http\Middleware\InventoryMiddleware;
use App\Http\Middleware\SellerMiddleware;
use App\Http\Resources\OperatorResource;
use App\Operator;
use Illuminate\Http\Request;

class OperatorController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', SellerMiddleware::class, InventoryMiddleware::class]);
    }

    public function index()
    {
        return OperatorResource::collection(Operator::where("status", 1)->get());
    }


    public function store(Request $request)
    {
        $request->validate([
            "name" => "required"
        ]);

        return new OperatorResource(Operator::create($request->all()));
    }

    public function update(Request $request, Operator $operator)
    {
        $request->validate([
            "name" => "required"
        ]);

        $operator->update($request->all());
        return new OperatorResource($operator);
    }

    public function destroy(Operator $operator)
    {

        $operator->update(["status" => 0]);
    }
}
