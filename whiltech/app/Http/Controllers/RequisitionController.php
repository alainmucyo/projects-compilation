<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AdminMiddleware;
use App\Http\Traits\MovementTrait;
use App\Product;
use App\ProductDetail;
use App\Receipt;
use App\Requisition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RequisitionController extends Controller
{
    use MovementTrait;

    public function __construct()
    {
        $this->middleware(['auth', AdminMiddleware::class]);
    }

    public function index()
    {
        return Session::get("requisitions");
    }

    public function requisitions()
    {
        return view("sales.index");
    }

    public function asked()
    {
        $requisitions = Requisition::latest()->get();
        $i = 1;
        return view("sales.askedReq", compact("requisitions", "i"));
    }

    public function requested()
    {
        $receipts = Receipt::latest()->get();
        $i = 1;
        return view("sales.requestedReq", compact("receipts", "i"));
    }

    public function store()
    {
        $year = now()->year;
        $moth = date("m");
        $day = date("d");
        $rand = mt_rand(1000, 9999);
        $random = $year . $moth . $day . $rand;
        $user_id = auth()->user()->id;
        $receipt = Receipt::create(["user_id" => $user_id, "number" => $random]);
        $products = Session::get("requisitions");

        foreach ($products as $product) {

            Requisition::create([
                "quantity" => $product['reqQuantity'],
                "details" => $product['reqDetails'],
                "product_id" => $product['product_id'],
                "receipt_id" => $receipt->id,
                "user_id" => $user_id,
                "product_name" => $product['product']['name'],
                "product_size" => $product['product']['size']['name'],
                "product_tag" => $product['product']['tag'],
                "product_description" => $product['product']['description']
            ]);
        }
        \session()->remove("requisitions");
        return Receipt::with("user", "requisitions")->find($receipt->id);

    }


    public function receipt(Receipt $receipt)
    {
        $receipt->requisitions;
        $requisitions = $receipt->requisitions;
        $i = 1;
        return view("sales.receipt", compact('receipt', 'requisitions', 'i'));
    }

    public function approve(Receipt $receipt)
    {
        $requisitions = $receipt->requisitions;
        foreach ($requisitions as $requisition) {
            $this->whereIsDispatched($requisition->product_id, $requisition->quantity);
            ProductDetail::create(["dispatched" => $requisition->quantity, "product_id" => $requisition->product_id]);
            $sale = Product::find($requisition->product_id);
            $oldSale = $sale->sale_stock;
            $sale->update(["sale_stock" => ($requisition->quantity + $oldSale)]);
        }
        $receipt->update(["approved" => 1]);
        Requisition::where("receipt_id", $receipt->id)->update(["approved" => 1]);
        return "ok";
    }

    public function update(Request $request, $requisition)
    {

        $request["product"] = Product::find($requisition);
        $request["size"] = $request["product"]->size;
        $request['product_id'] = $requisition;

        session()->push("requisitions", $request->all());
        return $request->all();
    }

    public function destroy($requisition)
    {
        // $requisition is index
        $products = session()->get("requisitions");
        unset($products[$requisition]);
        session()->remove("requisitions");
        foreach ($products as $product) {
            session()->push("requisitions", $product);
        }
        return $requisition;
    }

    public function updateSession(Request $request, $index)
    {
        // $requisition is index
        $products = session()->get("requisitions");
        $products[$index]['reqQuantity'] = $request['quantity'];
        session()->remove("requisitions");
        foreach ($products as $product) {
            session()->push("requisitions", $product);
        }
        return $index;
    }
}
