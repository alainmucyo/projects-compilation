<?php

namespace App\Http\Controllers;

use App\Http\Middleware\InventoryMiddleware;
use App\Http\Middleware\SellerMiddleware;
use App\Maintenance;
use App\Sale;
use App\SaleReceipt;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', SellerMiddleware::class, InventoryMiddleware::class]);
    }

    public function index()
    {
        $sales = SaleReceipt::whereBetween("updated_at", [Carbon::now()->startOfDay(),
            Carbon::now()->endOfDay()])->sum("paid_price");
        return view("admin.index", compact('sales'));
    }

    public function maintenance()
    {
        $maintenances = Maintenance::latest()->paginate(10);
        return view("admin.maintenance", compact('maintenances'));
    }

    public function unit()
    {
        return view("admin.unit");
    }

    public function operator()
    {
        return view("admin.operator");
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
