<?php

namespace App\Http\Controllers;

use App\Http\Traits\MovementTrait;
use App\Movement;
use App\Product;
use App\ProductDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class MovementController extends Controller
{
    use MovementTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createDetails(Request $request1)
    {
        foreach ($request1->all() as $request) {
            $oldMove = $this->getOldMovement($request['product_id']);

            $newClose = $oldMove->closing - $request['dispatched'];
            if ($newClose < 0) {
                $prod = Product::find($request['product_id'])->name;
                return response(["Not enough quantity for " . $prod], Response::HTTP_BAD_REQUEST);
            }
        }
        foreach ($request1->all() as $request) {
            $product = $request['product_id'];

            if ($request['dispatched'] && trim($request['dispatched']) != "") {
                $dispatched = $this->whereIsDispatched($product, $request['dispatched']);
                if (!$dispatched || $dispatched == null) {
                    $prod = Product::find($product)->name;
                    return response(["Not enough quantity for " . $prod], Response::HTTP_BAD_REQUEST);
                }
            }
            if ($request['received'] && trim($request['received']) != "") {
                $this->whereIsReceived($product, $request['received']);
            }
            ProductDetail::create($request);

        }
        session()->remove("products");
        return response("Update done successfully!");

    }
}
