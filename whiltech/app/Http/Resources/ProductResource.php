<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "size" => $this->size,
            "supplier" => $this->supplier,
            "category" => $this->category,
            "tag" => $this->tag,
            "description" => $this->description,
            "created_at" => $this->created_at

        ];
    }
}
