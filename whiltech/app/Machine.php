<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    protected $guarded = [];

    public function productions()
    {
        return $this->hasMany(Product::class);
    }

    public function maintenances(){
        return $this->hasMany(Maintenance::class);
    }
}
