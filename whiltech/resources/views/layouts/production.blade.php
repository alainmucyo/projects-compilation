<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("title")</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">

</head>
<body>

<div id="app">
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark bg-blue-600">
        <div class="navbar-brand wmin-0 mr-5">
            <a href="/home" class="d-inline-block">
                <img src="/global_assets/images/logo_light.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <span class=" ml-md-3 mr-md-auto">&nbsp;</span>
            <ul class="navbar-nav">


                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                       data-toggle="dropdown">
                        <img src="{{ auth()->user()->avatar?"/".auth()->user()->avatar:'/img/user.png' }}"
                             class="rounded-circle mr-2"
                             height="34"
                             alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                           class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Secondary navbar -->
    <div class="navbar navbar-expand-md navbar-light">
        <div class="text-center d-md-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                    data-target="#navbar-navigation">
                <i class="icon-unfold mr-2"></i>
                Navigation
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-navigation">
            <ul class="navbar-nav navbar-nav-highlight">

                <li class="nav-item">
                    <a href="/production" class="navbar-nav-link dash">
                        <i class="icon-home4 mr-2"></i>
                        Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/production/report/yearly" class="navbar-nav-link report">
                        <i class="icon-chart mr-2"></i>
                        Reports
                    </a>
                </li>
            </ul>

        </div>
    </div>

   

    <div class="page-content pt-0">

        <!-- Main content -->
        <div class="content-wrapper">

            <div class="content mt-5">
                <vue-progress-bar></vue-progress-bar>
                @yield("content")
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</div>
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; 2019
            </span>

    </div>
</div>
<!-- /footer -->

</body>
<script src="{{ asset('js/app.js') }}"></script>
<script src="/global_assets/js/main/jquery.min.js"></script>
<script src="/global_assets/js/main/bootstrap.bundle.min.js"></script>
<script src="/global_assets/js/plugins/loaders/blockui.min.js"></script>
<script src="/global_assets/js/plugins/ui/slinky.min.js"></script>
<script src="/global_assets/js/plugins/ui/ripple.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
<script src="/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script src="/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>

<script src="/assets/js/app.js"></script>
<script src="/global_assets/js/demo_pages/form_floating_labels.js"></script>
<script type="text/javascript">
    $(function () {
        $(".@yield('active2')").addClass("active")
    })
</script>
@yield("script")
@if(session('success'))
    <script>
        $.noty.defaults.killer = true;
        noty({
            text: '{{ session('success') }}!',
            layout: 'topCenter',
            type: 'success'
        });
    </script>
@endif
</html>
