@extends('layouts.app')
@section("title","Items")
@section("active","products")
@section('content')
    <Products></Products>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection