@extends("layouts.app")
@section("title","Units")
@section("active","unit")
@section("content")
    <div class="container">
        <Unit></Unit>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection