@extends("layouts.app")
@section("title","Maintenance")
@section("active","maintenance")
@section("content")
    <div class="container">
        <div class="card-group-control card-group-control-right">
            @foreach($maintenances as $maintenance)
                <div class="card mb-2">
                    <div class="card-header">
                        <h6 class="card-title">
                            <a class="text-default collapsed" data-toggle="collapse"
                               href="#question{{$maintenance->id}}" aria-expanded="false">
                                <i class="icon-help mr-2 text-slate"></i> {{ $maintenance->machine->name }}
                                , {{ $maintenance->machine->model }} - {{ $maintenance->machine->serial }}
                            </a>
                        </h6>
                    </div>

                    <div id="question{{ $maintenance->id }}" class="collapse" style="">
                        <div class="card-body">
                            {{ $maintenance->issue }}
                        </div>

                        <div class="card-footer bg-transparent d-sm-flex align-items-sm-center border-top-0 pt-0">
                            <span class="text-muted">Issue Reported: {{ $maintenance->created_at->diffForHumans() }}</span>

                            <div class="text-nowrap mb-0 ml-auto mt-2 mt-sm-0">
                                Reported By: <b>{{ $maintenance->reporter }}</b>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row justify-content-center">
            {!! $maintenances->links() !!}
        </div>
    </div>
@endsection