@extends("layouts.app")
@section("title","Operators")
@section("active","operators")
@section("content")
    <div class="container">
        <operator></operator>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection