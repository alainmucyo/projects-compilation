@extends('layouts.app')
@section("title","Stock Received ")
@section("active","dash")
@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card bg-primary">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">
                            {{ $dailyData }} Received
                        </h3>

                    </div>
                    <div>
                        Today Received Quantity
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-indigo">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{ $weeklyData }} Received</h3>

                    </div>
                    <div>
                        This week Received Quantity
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-success">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{ $monthlyData }} Received</h3>

                    </div>
                    <div>
                        This month Received Quantity.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-info">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">
                            {{$yearlyData }} Received
                        </h3>
                    </div>
                    <div>
                        This Year Received Quantity
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="header-elements float-right">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
            <h5 class="card-title">Receiving Statistics {{ auth()->user()->type==0?"For Requisitions":"" }}</h5>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12" style="overflow: scroll">
                    {!! $dailyChart->container() !!}
                </div>
                <div class="col-md-12" style="overflow: scroll">
                    {!! $monthlyChart->container() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Products
        </div>
        <div class="card-body table-responsive " style="margin-bottom: 20px">
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Tag</th>
                    <th>Units</th>
                    <th>Description</th>
                    <th>Supplier</th>
                    <th>Category</th>
                    <th>Stock Levels</th>
                    @if(auth()->user()->type !=2)
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{$product->name}}</td>
                        <td>{{ $product->tag }}</td>
                        <td>{{ $product->size->name }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->supplier }}</td>
                        <td>{{ $product->category }}</td>
                        @if(auth()->user()->type==1)
                            <td>
                                <label class="badge badge-{{ $product->movements->sortByDesc('id')->first()->closing<10?'danger':'success' }}">{{ $product->movements->sortByDesc('id')->first()->closing }}</label>
                            </td>
                        @else

                            <td>
                                <span v-show="false">{{ $product->movements->sortByDesc('id')->first()->closing }}</span>
                                <label class="badge badge-{{ $product->sale_stock<10?'danger':'success' }}">{{ $product->sale_stock }}</label>
                            </td>
                        @endif
                        @if(auth()->user()->type !=2)
                            <td class="text-center">
                                <div class="list-icons">
                                    <div class="dropdown">
                                        <a href="#" class="list-icons-item" data-toggle="dropdown"
                                           aria-expanded="false">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                                             style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 20px, 0px);">
                                            @if(auth()->user()->type==1)
                                                <a @click.prevent="addToSession({{ $product }})" href="#"
                                                   class="dropdown-item"><i class="icon-add-to-list"></i>Add To Edit
                                                    List</a>
                                            @else
                                                <a @click.prevent="addToReqSession({{ $product }})" href="#"
                                                   class="dropdown-item"><i class="icon-add-to-list"></i>Add To
                                                    Requisition
                                                    List</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@{{ sellerProduct.name }} product With current stock
                        level of
                        @{{sellerProduct.movements?sellerProduct.movements[sellerProduct.movements.length-1].closing:''
                        }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form @submit.prevent="submitReq">
                    <div class="modal-body">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Requisition Quantity</label>
                            <input type="number" class="form-control" v-model="form.reqQuantity"
                                   placeholder="Requisition Quantity" required>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Details (Optional)</label>
                            <textarea class="form-control" v-model="form.reqDetails"
                                      placeholder="Details (Optional)"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" :disabled="form.busy" class="btn btn-primary">@{{
                            form.busy?'Submitting...':'Submit'}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
    <script type="text/javascript" src="{{ asset('js/echarts-en.js') }}"></script>
    {!! $dailyChart->script() !!}
    {!! $monthlyChart->script() !!}
@endsection