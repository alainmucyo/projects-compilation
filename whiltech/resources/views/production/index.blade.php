@extends(auth()->user()->type==1?'layouts.production':'layouts.app')
@section("title","Productions")
@section("active","production")
@section("active2","dash")
@section("content")
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card bg-primary">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                                {{ $dailyData }} produced
                            </h3>
                            @if(auth()->user()->type !=2)
                                <div class="list-icons ml-auto">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                                    class="icon-cog3"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item" data-toggle="modal"
                                               data-target="#productionModal"><i class="icon-plus3"></i> Add New
                                                Production</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            Today Actual Production
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-indigo">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $weeklyData }} produced</h3>
                            @if(auth()->user()->type !=2)
                                <div class="list-icons ml-auto">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                                    class="icon-cog3"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item" data-toggle="modal"
                                               data-target="#productionModal"><i class="icon-plus3"></i> Add New
                                                Production</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            This week actual production
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-success">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $monthlyData }} produced</h3>
                            @if(auth()->user()->type !=2)
                                <div class="list-icons ml-auto">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                                    class="icon-cog3"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item" data-toggle="modal"
                                               data-target="#productionModal"><i class="icon-plus3"></i> Add New
                                                Production</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            This month actual production.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $machines->count() }} machines</h3>
                            <div class="list-icons ml-auto">
                                <div class="list-icons-item dropdown">
                                    <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                                class="icon-cog3"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="#" class="dropdown-item" data-toggle="modal"
                                           data-target="#machineModal"><i class="icon-plus3"></i> Add New Machine</a>
                                        @if(auth()->user()->type !=2)
                                            <a href="#" class="dropdown-item" data-toggle="modal"
                                               data-target="#maintenanceModal"><i class="icon-plus3"></i> Add
                                                Maintenance
                                                Problem</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            Machines
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="header-elements float-right">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
                <h5 class="card-title">Production Statistics</h5>
                @if(auth()->user()->type !=2)
                    <button class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#productionModal">New Production
                    </button>
                    <a href="#" class="btn btn-warning float-right" data-toggle="modal"
                       data-target="#maintenanceModal"><i class="icon-plus3"></i>
                        Maintenance
                    </a>
                @endif
                <a href="#" class="btn btn-success float-right" data-toggle="modal"
                   data-target="#machineModal"><i class="icon-plus3"></i>New Machine</a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" style="overflow: scroll">
                        {!! $weeklyChart->container() !!}
                    </div>
                    <div class="col-md-12" style="overflow: scroll">
                        {!! $monthlyChart->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                Machines
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>S/N</th>
                        <th>Model</th>
                        <th>Life (Yrs)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($machines as $machine)
                        @php( $life=new DateTime($machine->purchase_date))
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="/production/report/yearly?machine={{ $machine->id }}">{{ $machine->name }}</a>
                            </td>
                            <td>{{ $machine->serial }}</td>
                            <td>{{ $machine->model }}</td>
                            <td>{{ $life->diff(now())->y }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="machineModal" tabindex="-1" role="dialog" aria-labelledby="machineModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="machineModalLabel">Add New Machine</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="/production/machine">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Name</label>
                            <input type="text" class="form-control" name="name" required
                                   placeholder="Name"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Model</label>
                            <input type="text" class="form-control" name="model" required
                                   placeholder="Model"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Serial</label>
                            <input type="text" class="form-control" name="serial" required
                                   placeholder="Serial"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Purchased At</label>
                            <input type="date" class="form-control" name="purchase_date" required
                                   placeholder="Purchased At"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary legitRipple">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="productionModal" tabindex="-1" role="dialog" aria-labelledby="productionModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productionModalLabel">Add New Production</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="/production">
                    @csrf
                    <div class="modal-body" style="overflow:hidden;">
                        <div class="form-group">
                            <label for="select">Select Machine</label>
                            <select class="form-control select-search" style="width: 100%" name="machine_id"
                                    required
                            >
                                @foreach($machines as $machine)
                                    <option value="{{ $machine->id }}">{{ $machine->name }}
                                        , {{ $machine->model }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="select">Select Operator</label>
                            <select class="form-control select-search" style="width: 100%" name="operator_id" required
                            >
                                @foreach($operators as $operator)
                                    <option value="{{ $operator->id }}">{{ $operator->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="select">Select Product</label>
                            <select class="form-control select-search" style="width: 100%" name="product_id" required
                            >
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}
                                        , {{ $product->tag }} {{ $product->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="select">Select Raw Material</label>
                            <select class="form-control select-search" style="width: 100%" name="raw_id" required
                            >
                                @foreach($raw_products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}
                                        , {{ $product->tag }} {{ $product->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-row">

                            <div class="form-group form-group-float col-md-6">
                                <label class="form-group-float-label">Raw Material Qty</label>
                                <input type="number" class="form-control" name="raw" required
                                       placeholder="Raw Material Qty" min="0"/>
                            </div>
                            <div class="form-group form-group-float col-md-6">
                                <label class="form-group-float-label">Expected Qty</label>
                                <input type="number" class="form-control" name="expected" required
                                       placeholder="Expected Qty" min="0"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group form-group-float col-md-6">
                                <label class="form-group-float-label">Actual Qty</label>
                                <input type="number" class="form-control" name="actual" required
                                       placeholder="Actual Qty" min="0"/>
                            </div>
                            <div class="form-group form-group-float col-md-6">
                                <label class="form-group-float-label">Wasted Qty</label>
                                <input type="number" class="form-control" name="wasted" required
                                       placeholder="Wasted Qty" min="0"/>
                            </div>
                        </div>

                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">
                                Comment/Explanation</label>
                            <textarea class="form-control" name="comment"
                                      placeholder="Comment/Explanation"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary legitRipple">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="maintenanceModal" tabindex="-1" role="dialog" aria-labelledby="maintenanceModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="maintenanceModalLabel">Add Maintenance Problem</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="/production/maintenance">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="select">Select Machine</label>
                            <select class="form-control" style="width: 100%" id="select" name="machine_id"
                                    required
                            >
                                @foreach($machines as $machine)
                                    <option value="{{ $machine->id }}">{{ $machine->name }}
                                        , {{ $machine->model }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Issue Experienced</label>
                            <textarea class="form-control" name="issue"
                                      placeholder="Issue Experienced"></textarea>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Reported By</label>
                            <input type="text" class="form-control" name="reporter" required
                                   placeholder="Reported By"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary legitRipple">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
    <script type="text/javascript" src="/select/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.noty.packaged.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/echarts-en.js') }}"></script>
    {!! $weeklyChart->script() !!}
    {!! $monthlyChart->script() !!}
    <script type="text/javascript">
        $(function () {
            $('.select-search').select2({
                dropdownParent: $("#productionModal")
            });
            $('#select').select2({
                dropdownParent: $("#maintenanceModal")
            });

        });
    </script>
@endsection