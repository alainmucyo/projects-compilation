@extends(auth()->user()->type==1?'layouts.production':'layouts.app')
@section("title","Production Report In - ".$month)
@section("active","production")
@section("active2","report")
@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                Monthly Reports (2019) - {{ $month }}, @if($available) Machine: <b>{{ $machine->name?$machine->name:$machine }}
                    , {{ $machine->model?$machine->model:$machine }}@endif</b>
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Raw Material Quantity</th>
                        <th>Forecast Qty</th>
                        <th>Actual Qty</th>
                        <th>Wasted Qty</th>
                        <th>Production Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dates as $date => $productions)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="/production/report?day={{ $date }}{{ $available?'&machine='.$machine->id:'' }}">{{ $date }}</a></td>
                            <td>{{ $productions->sum('raw') }}</td>
                            <td>{{ $productions->sum('expected') }}</td>
                            <td>
                                <label class="badge badge-{{ $productions->sum('actual')<10?'danger':'success' }}">{{ $productions->sum('actual') }}</label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $productions->sum('wasted')<2?'success':'danger' }}">{{ $productions->sum('wasted') }}</label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $productions->count()<2?'danger':'success' }}">{{ $productions->count() }}</label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection