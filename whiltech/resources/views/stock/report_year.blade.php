@extends('layouts.app')
@section("title","Stock Report - ".now()->year)
@section("active","stock")
@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                Yearly Reports ({{ now()->year }})
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Month</th>
                        <th>Requisitions</th>
                        <th>Received</th>
                        <th>Dispatched</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($months as $month => $products)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>
                                <a href="/stock/report?month={{ $month }}">{{ $month }}</a>
                            </td>
                            <td>{{ \App\Requisition::whereYear("created_at",now()->year)
                            ->whereMonth("created_at",\Carbon\Carbon::parse($month)->month)
                            ->sum("quantity") }} </td>
                            <td>
                                {{ $products->sum('received') }}
                            </td>
                            <td>
                                {{ $products->sum("dispatched") }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection