@extends('layouts.app')
@section("title","Stock Report - ".$month)
@section("active","stock")
@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                Month Reports (2019) - {{ $month }}
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Day</th>
                        <th>Requisitions</th>
                        <th>Received</th>
                        <th>Dispatched</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($days as $day => $products)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>
                                <a href="/stock/report?day={{ $day }}">{{ $day }}</a>
                            </td>
                            <td>{{ \App\Requisition::whereYear("created_at",now()->year)
                            ->whereDate("created_at",$day)
                            ->sum("quantity") }} </td>
                            <td>
                                {{ $products->sum('received') }}
                            </td>
                            <td>
                                {{ $products->sum("dispatched") }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection