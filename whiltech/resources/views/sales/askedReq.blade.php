@extends('layouts.app')
@section("title"," Asked Requisition ")
@section("active","asked")
@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">
                Asked Requisition
            </h5>
        </div>
        <div class="card-body table-responsive">
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Product Name</th>
                    <th>Product Tag</th>
                    <th>Product Unit</th>
                    <th>Quantity</th>
                    <th>Details</th>
                    <th>Receipt #</th>
                    <th>Approved?</th>
                    <th>Requested At</th>
                </tr>
                </thead>
                <tbody>
                @foreach($requisitions as $requisition)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $requisition->product_name }}</td>
                        <td>{{ $requisition->product_tag }}</td>
                        <td>{{ $requisition->product_size }}</td>
                        <td>{{ $requisition->quantity }}</td>
                        <td>{{ $requisition->details?$requisition->details:'None' }}</td>
                        <td>{{ $requisition->receipt->number }}</td>
                        <td>
                            <label class="badge badge-{{ $requisition->approved?"success":"danger" }}"> {{ $requisition->approved?"Yes":"No" }}</label>
                        </td>
                        <td>{{ $requisition->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection