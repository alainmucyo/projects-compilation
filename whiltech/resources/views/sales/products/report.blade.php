@extends('layouts.app')
@section("title","Products Report")
@section("active","products")
@section("active2","report")
@section("content")

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h5>
            Products: <label class="badge badge-primary">{{$time}}</label>
                </h5>
                <div class="float-right">
                <a href="/sale/product/report" class="btn btn-{{ $time===now()->toDateString() ?'primary':'outline-primary' }}">Day</a>
                    <a href="/sale/product/report?type=week" class="btn btn-{{ $time=="This Week"?'primary':'outline-primary' }}">Week</a>
           <a href="/sale/product/report?type=month"  class="btn btn-{{ $time===now()->monthName?'primary':'outline-primary' }}">Month</a>
              <a href="/sale/product/report?type=year" class="btn btn-{{ $time===now()->year?'primary':'outline-primary' }}">Year</a></div>
            </div>
            
            <div class="card-body table-responsive " style="margin-bottom: 20px">
                    
                <br>
                
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                    
                        <th>Item</th>
                        <th>Unit</th>
                      
                        <th>Supplier</th>
                        <th>Category</th>
                        <th>Sold Times</th>
                        <th>Sold Quantities</th>
                        <th>Stock Levels For Selling</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product_id => $sales)
                    @php($product=App\Product::find($product_id))
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $product->name }}, {{ $product->tag }}
                                    . {{ $product->description }}</td>
                            <td>{{ $product->size->name }}</td>
                          
                            <td>{{ $product->supplier }}</td>
                            <td>{{ $product->category }}</td>
                        <td>
                            <span {{ $sale=$sales->count()}} 
                            class="badge badge-{{ $sale>5?'success':'danger' }}">{{$sale}}</span></td>
                            <td>
                                    <span {{ $sale=$sales->sum("quantity")}} 
                                    class="badge badge-{{ $sale>10?'success':'danger' }}">{{$sale}}</span></td>
                            <td>
                                <label class="badge badge-{{ $product->sale_stock<10?'danger':'success' }}">{{ $product->sale_stock }}</label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection