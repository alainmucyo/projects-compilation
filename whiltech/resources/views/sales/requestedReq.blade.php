@extends('layouts.app')
@section("title"," Asked Requisition")
@section("active","requested")
@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Asked Requisition</h5></div>
        <div class="card-body table-responsive">
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>User Name</th>
                    <th>Receipt Number</th>
                    <th>Requisition Number</th>
                    <th>Approved?</th>
                    <th>Requested At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($receipts as $receipt)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $receipt->user->name }}</td>
                        <td>#{{ $receipt->number }}</td>
                        <td>{{ $receipt->requisitions->count() }}</td>
                        <td>
                            <label class="badge badge-{{ $receipt->approved?"success":"danger" }}"> {{ $receipt->approved?"Yes":"No" }}</label>
                        </td>
                        <td>{{ $receipt->created_at }}</td>
                        <td>
                            <a href="/receipt/{{ $receipt->id }}" title="Show Products"
                               class="float-right btn btn-icon  bg-info legitRipple"
                            ><i
                                        class="icon-list3"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection