@extends("layouts.app")
@section("title","Selling Panel")
@section("active","selling")
@section("content")
    <Sale user="{{ auth()->user()->name }}"></Sale>
@endsection
@section("script")
    <script type="text/javascript" src="/select/select2.min.js"></script>
@endsection