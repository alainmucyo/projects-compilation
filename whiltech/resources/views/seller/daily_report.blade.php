@extends(auth()->user()->type==1?'layouts.sale':'layouts.app')
@section("title","Selling Reports - ".$month)
@section("active","selling")
@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"> Month {{ $month }}</h5>
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Selling Times</th>
                        <th>Required</th>
                        <th>Paid Money</th>
                        <th>Unpaid Money</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dates as $date => $receipts)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="/sale/report?date={{ $date }}">{{ $date }}</a></td>
                            <td>{{ $receipts->count() }}</td>
                            <td>
                                @php
                                    echo "{{" .$receipts->sum("required_price")." | currency}}"
                                @endphp
                            </td>
                            <td>
                                <label class="badge badge-{{ $receipts->sum("required_price")>$receipts->sum("paid_price")?'danger':'success' }}">
                                    @php
                                        echo "{{" .$receipts->sum("paid_price")." | currency}}"
                                    @endphp
                                </label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $receipts->sum("required_price")>$receipts->sum("paid_price")?'danger':'success' }}">
                                    @php
                                        echo "{{" .($receipts->sum("required_price") - $receipts->sum("paid_price"))." | currency}}"
                                    @endphp
                                </label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection