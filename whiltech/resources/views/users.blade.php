@extends('layouts.app')
@section("title","Users")
@section("active","users")
@section('content')
    <div class="container">
        <users></users>
    </div>

@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection