const mix = require('laravel-mix');


mix.js('resources/js/app.js', 'public/js')
    .babel('public/js/app.js', 'public/js/app.es5.js');
