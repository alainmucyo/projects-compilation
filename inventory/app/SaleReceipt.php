<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleReceipt extends Model
{
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }
}
