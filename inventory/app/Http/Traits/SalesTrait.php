<?php

namespace App\Http\Traits;

use App\Production;
use App\SaleReceipt;
use Carbon\Carbon;

trait SalesTrait
{

    public $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    public function weekly()
    {
        $days = array();
        $test = [0, 1, 2, 3, 4, 5, 6];
        foreach ($test as $i) {
            array_push($days,
                SaleReceipt::whereBetween("updated_at", [Carbon::now()->startOfWeek(),
                    Carbon::now()->endOfWeek()])->whereRaw('WEEKDAY(sale_receipts.updated_at)=' . $i)->sum("paid_price"));
        }

        return $days;
    }

    public function monthly()
    {
        $months = array();

        for ($i = 1; $i <= 12; $i++) {
            array_push($months, SaleReceipt::whereMonth("updated_at", $i)->sum("paid_price"));
        }

        return $months;

    }
    public function dailyData(){
       return SaleReceipt::whereBetween("updated_at", [Carbon::now()->startOfDay(),
            Carbon::now()->endOfDay()])->sum("paid_price");
    }
    public function weeklyData(){
       return SaleReceipt::whereBetween("updated_at", [Carbon::now()->startOfWeek(),
            Carbon::now()->endOfWeek()])->sum("paid_price");
    }
    public function monthlyData(){
       return SaleReceipt::whereBetween("updated_at", [Carbon::now()->startOfMonth(),
            Carbon::now()->endOfMonth()])->sum("paid_price");
    }
    public function yearlyData(){
       return SaleReceipt::whereBetween("updated_at", [Carbon::now()->startOfYear(),
            Carbon::now()->endOfYear()])->sum("paid_price");
    }
}