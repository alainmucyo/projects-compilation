<?php

namespace App\Http\Traits;

use App\Production;
use Carbon\Carbon;

trait ProductionTrait
{

    public $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    public function weekly()
    {
        $days = array();
        $test = [0, 1, 2, 3, 4, 5, 6];
        foreach ($test as $i) {
            array_push($days,
                Production::whereBetween("created_at", [Carbon::now()->startOfWeek(),
                    Carbon::now()->endOfWeek()])->whereRaw('WEEKDAY(productions.created_at)=' . $i)->sum("actual"));
        }

        return $days;
    }

    public function monthly()
    {
        $months = array();

        for ($i = 1; $i <= 12; $i++) {
            array_push($months, Production::whereMonth("created_at", $i)->sum("actual"));
        }

        return $months;

    }
    public function dailyData(){
       return Production::whereBetween("created_at", [Carbon::now()->startOfDay(),
            Carbon::now()->endOfDay()])->sum("actual");
    }
    public function weeklyData(){
       return Production::whereBetween("created_at", [Carbon::now()->startOfWeek(),
            Carbon::now()->endOfWeek()])->sum("actual");
    }
    public function monthlyData(){
       return Production::whereBetween("created_at", [Carbon::now()->startOfMonth(),
            Carbon::now()->endOfMonth()])->sum("actual");
    }
}