<?php

namespace App\Http\Traits;

use App\Movement;
use Illuminate\Support\Carbon;

trait MovementTrait
{
    public function getOldMovement($product)
    {

        $oldMove = Movement::whereBetween("created_at", [Carbon::now()->startOfDay(),
            Carbon::now()->endOfDay()])->where("product_id", $product);

        if ($oldMove->count() <= 0)
            $oldMove = $this->movementChange($product);
        else
            $oldMove = $oldMove->first();
        return $oldMove;

    }

    public function whereIsReceived($product, $received)
    {
        $oldMove = $this->getOldMovement($product);
        $newClose = $oldMove->closing + $received;
        $oldMove->update(['closing' => $newClose]);
        return Movement::all();

    }

    public function whereIsDispatched($product, $dispatched)
    {
        $oldMove = $this->getOldMovement($product);

        $newClose = $oldMove->closing - $dispatched;
        if ($newClose < 0)
            return null;
        $oldMove->update(['closing' => $newClose]);
        return Movement::all();

    }

    public function movementChange($product)
    {
        $oldMovement = Movement::where("product_id", $product)->orderByDesc("id")->first();
        return Movement::create(["opening" => $oldMovement->closing, "closing" => $oldMovement->closing, "product_id" => $product]);
    }
}