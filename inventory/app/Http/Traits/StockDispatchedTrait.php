<?php

namespace App\Http\Traits;

use App\ProductDetail;
use Carbon\Carbon;

trait StockDispatchedTrait
{

    public $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    public function weekly()
    {
        $days = array();
        $test = [0, 1, 2, 3, 4, 5, 6];
        foreach ($test as $i) {
            array_push($days,
                ProductDetail::whereBetween("created_at", [Carbon::now()->startOfWeek(),
                    Carbon::now()->endOfWeek()])->whereRaw('WEEKDAY(product_details.created_at)=' . $i)->sum("dispatched"));
        }

        return $days;
    }

    public function monthly()
    {
        $months = array();

        for ($i = 1; $i <= 12; $i++) {
            array_push($months, ProductDetail::whereMonth("created_at", $i)->sum("dispatched"));
        }

        return $months;

    }

    public function dailyData()
    {
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfDay(),
            Carbon::now()->endOfDay()])->sum("dispatched");
    }

    public function weeklyData()
    {
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfWeek(),
            Carbon::now()->endOfWeek()])->sum("dispatched");
    }

    public function monthlyData()
    {
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfMonth(),
            Carbon::now()->endOfMonth()])->sum("dispatched");
    }

    public function yearlyData()
    {
        return ProductDetail::whereBetween("created_at", [Carbon::now()->startOfYear(),
            Carbon::now()->endOfYear()])->sum("dispatched");
    }
}