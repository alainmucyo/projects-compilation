<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SellerMiddleware
{

    public function handle($request, Closure $next)
    {
        if (Auth::user()->type == 0)
            return redirect('/home');

        return $next($request);
    }
}
