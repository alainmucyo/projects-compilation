<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class InventoryMiddleware
{

    public function handle($request, Closure $next)
    {
        if (Auth::user()->type == 1)
            return redirect('/home');
        return $next($request);
    }
}
