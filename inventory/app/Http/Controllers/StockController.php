<?php

namespace App\Http\Controllers;

use App\Charts\LineChart;
use App\Http\Middleware\SellerMiddleware;
use App\Http\Traits\StockDispatchedTrait;
use App\Product;
use App\ProductDetail;
use Illuminate\Support\Carbon;

class StockController extends Controller
{
    use StockDispatchedTrait;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware([SellerMiddleware::class]);
    }
    public function index()
    {
        $products = Product::where("status",1)->get();
        $i = 1;
        $day = now()->toDateString();
        $dailyChart = new LineChart($this->days, $this->weekly(), "Daily Dispatched");
        $monthlyChart = new LineChart($this->months, $this->monthly(), "Monthly Dispatched");
        $monthlyData = $this->monthlyData();
        $yearlyData = $this->yearlyData();
        $weeklyData = $this->weeklyData();
        $dailyData = $this->dailyData();
        return view("stock.index", compact("products", "i", "day",'dailyChart', 'monthlyChart', 'weeklyData', 'monthlyData', 'dailyData', 'yearlyData'));
    }

    public function year()
    {
        $i = 1;
        $months = ProductDetail::whereYear("created_at", now()->year)->orderByDesc("created_at")->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->monthName;
            });

        return view("stock.report_year", compact('i', 'months'));
    }

    public function report()
    {
        $i = 1;
        if (\request("month")) {
            $month = \request('month');
            $days = ProductDetail::whereYear("created_at", now()->year)->whereMonth('created_at', Carbon::parse($month)->month)->orderByDesc("created_at")->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->toDateString();
                });
            return view("stock.report_month", compact('i', 'days', "month"));
        } else if (\request("day")) {
            $day = \request('day');
            $products = ProductDetail::whereDate("created_at", $day)->get()
                ->groupBy(function ($val) {
                    return $val->product_id;
                });

            return view("stock.report_day", compact('i', 'products', 'day'));
        } else {
            return redirect("/stock/report/yearly");
        }

    }
}
