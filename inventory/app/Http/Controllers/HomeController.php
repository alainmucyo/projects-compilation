<?php

namespace App\Http\Controllers;

use App\Charts\LineChart;
use App\Http\Traits\StockReceivedTrait;
use App\Product;

class HomeController extends Controller
{
    use StockReceivedTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->user()->type == 1) {
            return view('home');
        } else
            $products = Product::where("category", "Product")->where("status",1)->get();
        $i = 1;
        $dailyChart = new LineChart($this->days, $this->weekly(), "Daily Received");
        $monthlyChart = new LineChart($this->months, $this->monthly(), "Monthly Received");
        $yearlyData = $this->yearlyData();
        $monthlyData = $this->monthlyData();
        $weeklyData = $this->weeklyData();
        $dailyData = $this->dailyData();

        return view('dashboard', compact('products', 'i', 'dailyChart', 'monthlyChart', 'weeklyData', 'monthlyData', 'dailyData', 'yearlyData'));
    }

    public function dashboard()
    {
        $products = Product::where("status",1)->get();
        $i = 1;
        $dailyChart = new LineChart($this->days, $this->weekly(), "Daily Received");
        $monthlyChart = new LineChart($this->months, $this->monthly(), "Monthly Received");
        $yearlyData = $this->yearlyData();
        $monthlyData = $this->monthlyData();
        $weeklyData = $this->weeklyData();
        $dailyData = $this->dailyData();

        return view('dashboard', compact('products', 'i', 'dailyChart', 'monthlyChart', 'weeklyData', 'monthlyData', 'dailyData', 'yearlyData'));
    }

    public function products()
    {
        return view("product.index");
    }

}
