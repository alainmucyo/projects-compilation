<?php

namespace App\Http\Controllers;

use App\Charts\LineChart;
use App\Http\Middleware\SellerMiddleware;
use App\Http\Traits\MovementTrait;
use App\Http\Traits\ProductionTrait;
use App\Machine;
use App\Maintenance;
use App\Operator;
use App\Product;
use App\ProductDetail;
use App\Production;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductionController extends Controller
{
    use MovementTrait;
    use ProductionTrait;

    public function __construct()
    {
        $this->middleware(['auth', SellerMiddleware::class]);
    }

    public function index()
    {
        $weeklyChart = new LineChart($this->days, $this->weekly(), "Weekly Actual Produced");
        $monthlyChart = new LineChart($this->months, $this->monthly(), "Monthly Actual Produced.");
        $machines = Machine::where("status",1)->get();
        $products = Product::where("category", "Product")->where("status",1)->get();
        $operators = Operator::where("status", 1)->get();
        $i = 1;
        $monthlyData = $this->monthlyData();
        $weeklyData = $this->weeklyData();
        $dailyData = $this->dailyData();
        return view('production.index',
            compact('machines', 'i', 'products', 'operators', 'weeklyChart', 'monthlyChart', 'weeklyData', 'monthlyData', 'dailyData'));
    }

    public function storeMachine(Request $request)
    {
        $request->validate([
            "name" => "required",
        ]);

        Machine::create($request->all());
        session()->flash("success", "Machine Created Successfully!");
        return redirect()->back();

    }

    public function store(Request $request)
    {

        $request->validate([
            "expected" => "required",
            "actual" => "required",
        ]);

        $this->whereIsReceived($request['product_id'], $request['actual']);
        $detail = ProductDetail::create(["product_id" => $request['product_id'], "received" => $request['actual']]);
        $request['product_detail_id'] = $detail->id;
        Production::create($request->all());
        session()->flash("success", "Production Created Successfully!");
        return redirect()->back();
    }

    public function storeMaintenance(Request $request)
    {
        $request->validate([
            "reporter" => "required",
        ]);
        Maintenance::create($request->all());
        session()->flash("success", "Maintenance Submitted Successfully!");
        return redirect()->back();
    }

    public function yearly()
    {
        if (\request("machine")) {
            $machine = Machine::find(\request("machine"));
            $available = true;
            $production = Production::whereYear("created_at", now()->year)->where("machine_id", \request("machine"));
        } else {
            $available = false;
            $production = Production::whereYear("created_at", now()->year);
        }
        $months = $production->orderByDesc("created_at")->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->monthName;
            });
        $i = 1;
        return view("production.report_year", compact('months', 'i', 'machine', 'available'));
    }

    public function report()
    {
        $i = 1;
        if (\request("machine")) {
            $available = true;
            $machine = Machine::find(\request("machine"));
            $production = Production::whereYear("created_at", now()->year)->where("machine_id", \request("machine"));
        } else {
            $available = false;
            $production = Production::whereYear("created_at", now()->year);
        }

        if (\request('month')) {
            $month = \request('month');
            $dates = $production->whereMonth('created_at', Carbon::parse($month)->month)->orderByDesc("created_at")->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->toDateString();
                });

            return view('production.report_month', compact('dates', 'i', 'month', 'machine', 'available'));
        } else if (\request('day')) {
            $day = \request('day');
            $productions = $production->whereDate("created_at", $day)->get();
            return view("production.report_day", compact('productions', 'day', 'i', 'machine', 'available'));
        } else
            return redirect("/production/report/yearly?machine=" . $machine);

    }

    public function update(Request $request, Production $production)
    {
        $oldActual = $production->actual;
        $newActual = ($request['actual'] - $oldActual);
        $this->whereIsReceived($request->product_id, $newActual);
        if ($product = ProductDetail::find($production->product_detail_id)) {
            $product->update(["received" => $request['actual'], "product_id" => $request['product_id']]);
        } else {
            ProductDetail::create(["received" => $request['actual'], "product_id" => $request['product_id']]);
        }
        $production->update($request->all());
        return redirect()->back();
    }

    public function destroy($id)
    {
        //
    }
}
