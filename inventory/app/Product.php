<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function movements()
    {
        return $this->hasMany(Movement::class);
    }

    public function requisitions()
    {
        return $this->hasMany(Requisition::class);
    }

    public function productions()
    {
        return $this->hasMany(Production::class);
    }
}
