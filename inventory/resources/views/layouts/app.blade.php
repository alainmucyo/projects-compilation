<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("title")</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">

</head>
<body>

<div id="app">
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark bg-blue-600">
        <div class="navbar-brand wmin-0 mr-5">
            <a href="/home" class="d-inline-block">
                <img src="/global_assets/images/logo_light.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">

            @if(auth()->user()->type==1)
                <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto">{{ \App\Requisition::all()->count() }} Requisitions</span>
            @else
                <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto">{{ auth()->user()->requisitions->count() }} requisitions</span>
            @endif
            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                       data-toggle="dropdown">
                        <img src="{{ auth()->user()->avatar?"/".auth()->user()->avatar:'/img/user.png' }}"
                             class="rounded-circle mr-2"
                             height="34"
                             alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/profile" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                           class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Secondary navbar -->
    <div class="navbar navbar-expand-md navbar-light">
        <div class="text-center d-md-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                    data-target="#navbar-navigation">
                <i class="icon-unfold mr-2"></i>
                Navigation
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-navigation">
            @if(auth()->user()->type!=2)
                <ul class="navbar-nav navbar-nav-highlight">

                    @if(auth()->user()->type==1)
                        <li class="nav-item ">
                            <a href="/dashboard" class="navbar-nav-link dash">
                                <i class="icon-home4 mr-2"></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/products" class="navbar-nav-link products">
                                <i class="icon-package mr-2"></i>
                                Products
                            </a>
                        </li>
                        <Added></Added>
                        <li class="nav-item">
                            <a href="/requested-requisitions" class="navbar-nav-link requested ">
                                <i class="icon-question7 mr-2"></i>
                                Requested Requisitions &nbsp;<span
                                        class="badge badge-primary"
                                        style="margin-top: 13px">{{ \App\Requisition::where("approved",0)->count() }}</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="navbar-nav-link dropdown-toggle stock" data-toggle="dropdown">
                                <i class="icon-store2 mr-2"></i>
                                Stock
                            </a>
                            <div class="dropdown-menu">
                                <a href="/stock" class="dropdown-item">
                                    Stock
                                </a>
                                <a href="/stock/report/yearly" class="dropdown-item">
                                    Stock Reports
                                </a>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="/home" class="navbar-nav-link dash">
                                <i class="icon-home4 mr-2"></i>
                                Dashboard
                            </a>
                        </li>
                        <req-added></req-added>
                        <li class="nav-item">
                            <a href="/asked-requisitions" class="navbar-nav-link asked">
                                <i class="icon-question7 mr-2"></i>
                                Asked Requisitions &nbsp;<span
                                        class="badge badge-primary"
                                        style="margin-top: 13px">{{ auth()->user()->requisitions->where("approved",0)->count() }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="navbar-nav-link" data-toggle="modal"
                               data-target="#payModal">
                                <i class="icon-cash4 mr-2"></i>
                                Pay
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="navbar-nav-link dropdown-toggle selling" data-toggle="dropdown">
                                <i class="icon-cash2 mr-2"></i>
                                Selling
                            </a>
                            <div class="dropdown-menu">
                                <a href="/sale" class="dropdown-item">
                                    Selling
                                </a>
                                <a href="/sale/report" class="dropdown-item">
                                    Selling Reports
                                </a>
                            </div>
                        </li>
                    @endif

                </ul>
            @else
                <ul class="navbar-nav navbar-nav-highlight">

                    <li class="nav-item">
                        <a href="/admin" class="navbar-nav-link dash">
                            <i class="icon-home4 mr-2"></i>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="navbar-nav-link dropdown-toggle stock" data-toggle="dropdown">
                            <i class="icon-store2 mr-2"></i>
                            Stock
                        </a>
                        <div class="dropdown-menu">
                            <a href="/stock" class="dropdown-item">
                                Stock
                            </a>
                            <a href="/stock/report/yearly" class="dropdown-item">
                                Stock Reports
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="navbar-nav-link dropdown-toggle production" data-toggle="dropdown">
                            <i class="icon-gear mr-2"></i>
                            Production
                        </a>
                        <div class="dropdown-menu">
                            <a href="/production" class="dropdown-item">
                                Production
                            </a>
                            <a href="/production/report/yearly" class="dropdown-item">
                                Production Reports
                            </a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a href="#" class="navbar-nav-link dropdown-toggle selling" data-toggle="dropdown">
                            <i class="icon-cash2 mr-2"></i>
                            Selling
                        </a>
                        <div class="dropdown-menu">
                            <a href="/sale" class="dropdown-item">
                                Selling
                            </a>
                            <a href="/sale/report" class="dropdown-item">
                                Selling Reports
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/users" class="navbar-nav-link users">
                            <i class="icon-users4 mr-2"></i>
                            Users
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/unit" class="navbar-nav-link unit">
                            <i class="icon-package mr-2"></i>
                            Unit
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/maintenance" class="navbar-nav-link maintenance">
                            <i class="icon-warning2 mr-2"></i>
                            Maintenance
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/operators" class="navbar-nav-link operators">
                            <i class="icon-user-tie mr-2"></i>
                            Operators
                        </a>
                    </li>

                </ul>
            @endif

        </div>
    </div>
    <div class="page-content pt-0">

        <div class="content-wrapper">

            <div class="content mt-5">
                <vue-progress-bar></vue-progress-bar>
                @yield("content")
            </div>

        </div>

    </div>

    <div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="payModalLabel">Paying</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form @submit.prevent="storePayment">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Receipt #</label>
                            <input type="number" min="0" class="form-control" v-model="pay.receipt" required
                                   placeholder="Receipt #"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Amount Payed</label>
                            <input type="number" class="form-control" required v-model="pay.amount"
                                   placeholder="Amount Payed"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" :disabled="pay.busy" class="btn btn-primary">@{{
                            pay.busy?'Submitting...':'Submit'}}
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; 2019
            </span>

    </div>
</div>

</body>
<script src="{{ asset('js/app.js') }}"></script>
<script src="/global_assets/js/main/jquery.min.js"></script>
<script src="/global_assets/js/main/bootstrap.bundle.min.js"></script>
<script src="/global_assets/js/plugins/loaders/blockui.min.js"></script>
<script src="/global_assets/js/plugins/ui/slinky.min.js"></script>
<script src="/global_assets/js/plugins/ui/ripple.min.js"></script>

<script src="/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
<script src="/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script src="/global_assets/js/plugins/forms/styling/switchery.min.js"></script>

<script src="/assets/js/app.js"></script>
<script src="/global_assets/js/demo_pages/dashboard.js"></script>
<script src="/global_assets/js/demo_pages/form_floating_labels.js"></script>
<script type="text/javascript">
    $(function () {
        $(".@yield('active')").addClass("active")
    })
</script>
@yield("script")
</html>

