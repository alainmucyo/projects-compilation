<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Home</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">

</head>
<body>

<div id="app">
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark bg-blue-600">
        <div class="navbar-brand wmin-0 mr-5">
            <a href="index.html" class="d-inline-block">
                <img src="/global_assets/images/logo_light.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">

            <span class="ml-md-3 mr-md-auto">&nbsp;</span>

            <ul class="navbar-nav">


                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                       data-toggle="dropdown">
                        <img src="{{ auth()->user()->avatar?"/".auth()->user()->avatar:'/img/user.png' }}"
                             class="rounded-circle mr-2"
                             height="34"
                             alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                           class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="page-content pt-0">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">
                <div class="container" style="margin-top: 5%">
                    <div class="row justify-content-center">
                        <div class="col-md-4">

                            <div class="card text-center" style="min-height: 450px;">
                                <div class="card-body">
                                    <i class="icon-package icon-5x text-success border-success border-3 rounded-round p-3 mb-3"></i>
                                    <h4 class="card-title">Inventory & Stores</h4>
                                    <h5 class="mb-3">For all Store Inventories, stock levels, and consumables.</h5>
                                    <br><br><br>
                                    <a href="/dashboard" class="btn bg-success">Open <i
                                                class="icon-arrow-right14 ml-2"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="card card-body text-center" style="min-height: 450px;">
                                <div>
                                    <i class="icon-gear icon-5x text-primary border-primary border-3 rounded-round p-3 mb-3"></i>
                                    <h4 class="card-title">Production</h4>
                                    <h5 class="mb-3">
                                        Production, Preventive Maxinstances and technical life</h5>
                                    <br><br><br>
                                    <a href="/production" class="btn bg-blue-400">Open <i
                                                class="icon-arrow-right14 ml-2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</div>
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; 2019
            </span>

    </div>
</div>
<!-- /footer -->
</body>
<script src="/global_assets/js/main/jquery.min.js"></script>
<script src="/global_assets/js/main/bootstrap.bundle.min.js"></script>

@yield("script")
</html>
