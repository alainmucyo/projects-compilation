@extends(auth()->user()->type==1?'layouts.production':'layouts.app')
@section("title","Production Report At - ".$day)
@section("active","production")
@section("active2","report")
@section("content")

    <div class="container">
        <div class="card">
            <div class="card-header">
                Daily Report, at -  <label class="badge badge-primary" style="font-size: 13px">{{ $day }}</label> @if($available) Machine: <b>{{ $machine->name?$machine->name:$machine }}
                    , {{ $machine->model?$machine->model:$machine }}@endif</b>
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Product</th>
                        <th>Forecast Qty</th>
                        <th>Actual Qty</th>
                        <th>Wasted Qty</th>
                        <th>Comment</th>
                        <th>Shift</th>
                        <th>Machine</th>
                        <th>Operator</th>
                        @if(auth()->user()->type==1)
                            <th>Action</th> @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($productions as $production)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $production->product->name }}, {{ $production->product->tag }}
                                . {{ $production->product->description }}</td>
                            <td>{{ $production->expected }}</td>
                            <td>
                                <label class="badge badge-{{ $production->actual<5?'danger':'success' }}">{{ $production->actual }}</label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $production->wasted<1?'success':'danger' }}">{{ $production->wasted }}</label>
                            </td>
                            <td class="{{ $production->comment?'':'text-danger' }}">{{ $production->comment?$production->comment:'None' }}</td>
                            <td>{{ $production->shift }}</td>
                            <td>{{ $production->machine->name }}, {{ $production->machine->model }}</td>
                            <td>{{ $production->operator->name }}</td>
                            @if(auth()->user()->type==1)
                                <td>
                                    <button class="btn btn-icon btn-outline-info"
                                            @click="editProduction({{$production}})">
                                        <i class="icon-pencil4"></i></button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if(auth()->user()->type==1)
        <div class="modal fade" id="productionEditModal" tabindex="-1" role="dialog"
             aria-labelledby="productionEditModalLabel"
             aria-hidden="true" v-if="allProductions">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="productionEditModalLabel">Edit Production</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" :action="'/production/'+allProductions.id">
                        {{ method_field("put") }}
                        @csrf
                        <div class="modal-body" style="overflow:hidden;">
                            <div class="form-group">
                                <label for="select">Select Machine</label>
                                <select class="form-control select-search" style="width: 100%"
                                        v-model="production.machine_id"
                                        name="machine_id"
                                        required
                                >
                                    <option v-for="machine in machines" :value="machine.id">@{{ machine.name }}
                                        , @{{ machine.model }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select">Select Operator</label>
                                <select class="form-control select-search" style="width: 100%"
                                        v-model="production.operator_id"
                                        name="operator_id" required
                                >
                                    <option v-for="operator in operators" :value="operator.id">@{{ operator.name }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select">Select Product</label>
                                <select class="form-control select-search" style="width: 100%"
                                        v-model="production.product_id"
                                        name="product_id" required
                                >
                                    <option v-for="product in products" :value="product.id">@{{ product.name }}
                                        , @{{ product.tag }} @{{ product.description }}
                                    </option>

                                </select>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="shift">Shift</label>
                                    <select id="shift" class="form-control" name="shift" v-model="production.shift"
                                            required
                                    >
                                        <option value="Day">Day</option>
                                        <option value="Night">Night</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Expected Qty</label>
                                    <input type="number" class="form-control" v-model="production.expected"
                                           name="expected"
                                           required
                                           placeholder="Expected Qty" min="0"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Actual Qty</label>
                                    <input type="number" class="form-control" v-model="production.actual" name="actual"
                                           required
                                           placeholder="Actual Qty" min="0"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Wasted Qty</label>
                                    <input type="number" class="form-control" v-model="production.wasted" name="wasted"
                                           required
                                           placeholder="Wasted Qty" min="0"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>
                                    Comment/Explanation</label>
                                <textarea class="form-control" name="comment"
                                          placeholder="Comment/Explanation" v-model="production.comment"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close
                            </button>
                            <button type="submit" :disabled="form.busy" class="btn btn-primary">@{{
                                production.busy?'Submitting...':'Submit'}}
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
    <script type="text/javascript" src="/select/select2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.select-search').select2({
                dropdownParent: $("#productionEditModal")
            });
        })
    </script>
@endsection