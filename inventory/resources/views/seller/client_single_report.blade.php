@extends(auth()->user()->type==1?'layouts.sale':'layouts.app')
@section("title","Client ".$client->name." Payment Report")
@section("active","selling")
@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title"> Client <b>{{ $client->name }}</b> with phone of <b>{{ $client->phone }}</b></h5>
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Invoice #</th>
                        <th>Products Number</th>
                        <th>Amount Required</th>
                        <th>Amount Paid</th>
                        <th>Left Amount</th>
                        <th>Payment Method</th>
                        <th>Total Quantity</th>
                        <th>Done At</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($receipts as $receipt)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="#" @click.prevent="sales({{$receipt}})">{{ $receipt->number }}</a></td>
                            <td>
                                <label class="badge badge-{{  $receipt->sales->count()>5?'success':'danger' }}"></label> {{  $receipt->sales->count() }}
                            </td>
                            <td>
                                @php
                                    echo "{{" .$receipt->required_price." | currency}}"
                                @endphp
                            </td>
                            <td>
                                <label class="badge badge-{{ $receipt->required_price>$receipt->paid_price?'danger':'success' }}">
                                    @php
                                        echo "{{" .$receipt->paid_price." | currency}}"
                                    @endphp
                                </label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $receipt->required_price>$receipt->paid_price?'danger':'success' }}">
                                    @php
                                        echo "{{" .($receipt->required_price - $receipt->paid_price)." | currency}}"
                                    @endphp
                                </label>
                            </td>
                            <td>{{ $receipt->payment }}</td>
                            <td>{{ $receipt->sales->sum("quantity") }}</td>
                            <td>{{ $receipt->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="modal fade" id="salesModal" tabindex="-1" role="dialog" aria-labelledby="salesModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="salesModalLabel">Invoice #@{{ client.number }} Sales</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table" v-if="clientSales.length>0">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Done At</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(sale,index) in clientSales">
                            <td>@{{ index+1 }}</td>
                            <td>@{{ sale.product.name+', '+sale.product.tag+' - '+sale.product.description }}</td>
                            <td>@{{ sale.quantity }}</td>
                            <td>@{{ sale.unit_price | currency }}</td>
                            <td>@{{ sale.created_at | moment("dddd, MMMM Do YYYY") }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection