@extends(auth()->user()->type==1?'layouts.sale':'layouts.app')
@section("active","selling")
@section("title","Selling Report - ".$day)
@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                 Date At {{ $day }}
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Payment</th>
                        <th>Selling Times</th>
                        <th>Required</th>
                        <th>Paid Money</th>
                        <th>Unpaid Money</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $payment => $receipts)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="/sale/report?payment={{ $payment }}">{{ $payment }}</a></td>
                            <td>{{ $receipts->count() }}</td>
                            <td>
                                @php
                                    echo "{{" .$receipts->sum("required_price")." | currency}}"
                                @endphp
                            </td>
                            <td>
                                <label class="badge badge-{{ $receipts->sum("required_price")>$receipts->sum("paid_price")?'danger':'success' }}">
                                    @php
                                        echo "{{" .$receipts->sum("paid_price")." | currency}}"
                                    @endphp
                                </label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $receipts->sum("required_price")>$receipts->sum("paid_price")?'danger':'success' }}">
                                    @php
                                        echo "{{" .($receipts->sum("required_price") - $receipts->sum("paid_price"))." | currency}}"
                                    @endphp
                                </label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection