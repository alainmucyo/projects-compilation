@extends(auth()->user()->type==1?'layouts.sale':'layouts.app')
@section("title","Sales")
@section("active","selling")
@section("content")
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card bg-primary">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                                @php
                                    echo "{{".$dailyData." | currency}}"
                                @endphp
                            </h3>
                            <div class="list-icons ml-auto">
                            </div>
                        </div>
                        <div>
                            Today Income
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-indigo">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                            @php
                                echo "{{".$weeklyData." | currency}}"
                            @endphp
                        </div>
                        <div>
                            This Week Income
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-success">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                                @php
                                    echo "{{".$monthlyData." | currency}}"
                                @endphp
                            </h3>
                        </div>
                        <div>
                            This Month Income.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                                @php
                                    echo "{{".$yearlyData." | currency}}"
                                @endphp</h3>
                        </div>
                        <div>
                            This Year Income
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">

            <div class="card-header">
                <div class="header-elements float-right">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
                <h5 class="card-title">Selling Statistics</h5>

                <div class="btn-group btn-group-toggle float-right">
                    @if( auth()->user()->type==0)
                        <a href="/sale/selling" class="btn bg-success legitRipple">
                            <span class="icon-arrow-right15"></span> Start Selling
                        </a>
                    @endif
                    <button class="btn bg-blue-600 legitRipple" data-toggle="modal"
                            data-target="#clientModal">
                        <span class="icon-plus2"></span> New Client
                    </button>
                </div>
                <br>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" style="overflow: scroll">
                        {!! $weeklyChart->container() !!}
                    </div>
                    <div class="col-md-12" style="overflow: scroll">
                        {!! $monthlyChart->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                Clients
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Client Names</th>
                        <th>Client Email</th>
                        <th>Client Contact</th>
                        <th>Times Bought</th>
                        <th>Required Amount</th>
                        <th>Payed Amount</th>
                        <th>Left Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="/sale/report/{{ $client->id }}">{{ $client->name }}</a></td>
                            <td class="{{ $client->email?'':'text-danger' }}">{{ $client->email?$client->email:'None' }}</td>
                            <td>{{ $client->phone }}</td>
                            <td>
                                <label class="badge badge-{{ $client->receipts->count()>1?'success':'danger' }}"></label> {{ $client->receipts->count() }}
                            </td>
                            <td>
                                @php
                                    echo "{{" .$client->receipts->sum("required_price")." | currency}}"
                                @endphp
                            </td>
                            <td>
                                <label class="badge badge-{{ $client->receipts->sum("required_price")>$client->receipts->sum("paid_price")?"danger":"success" }}">
                                    @php
                                        echo "{{" .$client->receipts->sum("paid_price")." | currency}}"
                                    @endphp
                                </label>
                            </td>
                            <td>
                                <label class="badge badge-{{ $client->receipts->sum("required_price")>$client->receipts->sum("paid_price")?"danger":"success" }}">
                                    @php
                                        echo "{{" .($client->receipts->sum("required_price") - $client->receipts->sum("paid_price"))." | currency}}"
                                    @endphp
                                </label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="clientModal" tabindex="-1" role="dialog" aria-labelledby="clientModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="clientModalLabel">Add New Client</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="/sale/client">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Client Names</label>
                            <input type="text" class="form-control" name="name" required
                                   placeholder="Client Names"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Company Name</label>
                            <input type="text" class="form-control" name="company" placeholder="Company Name"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Client Email</label>
                            <input type="email" class="form-control" name="email"
                                   placeholder="Client Email"/>
                        </div>
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label">Phone Contact</label>
                            <input type="tel" class="form-control" name="phone" required
                                   placeholder="Phone Contact"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary legitRipple" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary legitRipple">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.noty.packaged.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/echarts-en.js') }}"></script>
    {!! $weeklyChart->script() !!}
    {!! $monthlyChart->script() !!}
@endsection