@extends('layouts.app')
@section("title","Stock Dispatched")
@section("active","stock")
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card bg-primary">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                                {{ $dailyData }} Dispatched
                            </h3>

                        </div>
                        <div>
                            Today Dispatched Quantity
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-indigo">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $weeklyData }} Dispatched</h3>

                        </div>
                        <div>
                            This week Dispatched Quantity
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-success">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $monthlyData }} Dispatched</h3>

                        </div>
                        <div>
                            This month Dispatched Quantity.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">
                                {{$yearlyData }} Dispatched
                            </h3>
                        </div>
                        <div>
                            This Year Dispatched Quantity
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="header-elements float-right">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
                <h5 class="card-title">Receiving Statistics</h5>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" style="overflow: scroll">
                        {!! $dailyChart->container() !!}
                    </div>
                    <div class="col-md-12" style="overflow: scroll">
                        {!! $monthlyChart->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Product - <label class="badge badge-primary">{{ $day }}</label></h5>
            </div>
            <div class="card-body table-responsive">
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Item</th>
                        <th>Opening</th>
                        <th>Requisition</th>
                        <th>Dispatched</th>
                        <th>Dispatched</th>
                        <th>Closing</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $product->name }}, {{ $product->tag }}
                                . {{ $product->description }}</td>
                            @php($test=\App\Movement::where("product_id",$product->id)->whereDate("created_at",$day)->latest()->first())
                            <td>{{ $test?$test->opening:$product->movements->last()->opening}}</td>
                            <td>{{ \App\Requisition::where("product_id",$product->id)->whereDate("created_at",$day)->sum("quantity") }}</td>
                            <td>{{ \App\ProductDetail::whereDate("created_at",$day)->where("product_id",$product->id)->sum("received") }}</td>
                            <td>{{ \App\ProductDetail::whereDate("created_at",$day)->where("product_id",$product->id)->sum("dispatched") }}</td>
                            <td>{{ $test?$test->closing:$product->movements->last()->opening }}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
    <script type="text/javascript" src="{{ asset('js/echarts-en.js') }}"></script>
    {!! $dailyChart->script() !!}
    {!! $monthlyChart->script() !!}
@endsection