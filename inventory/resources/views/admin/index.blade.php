@extends("layouts.app")
@section("title","Admin Received Quantity")
@section("active","dash")
@section("content")
    <div class="row">
        <div class="col-md-4">
            <div class="card bg-primary">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">
                        @php
                         echo "{{".$sales." | currency}}"
                        @endphp
                        </h3>
                        <div class="list-icons ml-auto">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon-cog3"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item"><i class="icon-sync"></i> Option</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        Amount Received From Selling
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card bg-indigo">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{ $sales }}</h3>
                        <div class="list-icons ml-auto">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon-cog3"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        Current server load
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="card bg-success">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">49.4%</h3>
                        <div class="list-icons ml-auto">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon-cog3"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        Current server load
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection