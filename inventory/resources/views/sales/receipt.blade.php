@extends('layouts.app')
@section("title"," Receipt #". $receipt->number)
@section("active","asked")
@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Receipt <b>#{{ $receipt->number }}</b> Sent By <b>{{ $receipt->user->name }}</b> At
                <b>{{ $receipt->created_at }}</b></h5></div>
        <div class="card-body table-responsive">
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Product Name</th>
                    <th>Product Tag</th>
                    <th>Product Unit</th>
                    <th>Quantity</th>
                    <th>Details</th>
                    <th>Receipt #</th>
                    <th>Approved?</th>
                    <th>Requested At</th>
                </tr>
                </thead>
                <tbody>
                @foreach($requisitions as $requisition)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $requisition->product_name }}</td>
                        <td>{{ $requisition->product_tag }}</td>
                        <td>{{ $requisition->product_size }}</td>
                        <td>{{ $requisition->quantity }}</td>
                        <td>{{ $requisition->details?$requisition->details:'None' }}</td>
                        <td>{{ $requisition->receipt->number }}</td>
                        <td>
                            <label class="badge badge-{{ $requisition->approved?"success":"danger" }}"> {{ $requisition->approved?"Yes":"No" }}</label>
                        </td>
                        <td>{{ $requisition->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(!$receipt->approved)
            <div class="card-footer">
                <button class="btn btn-primary float-right" @click="approveReq({{$receipt->id}})">Approve</button>
            </div>
        @endif
    </div>
    @if(!$receipt->approved)
        <div class="card" id="reqPrint" v-show="false">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-4">
                            INVENTORY
                            <ul class="list list-unstyled mb-0">
                                <li>KIGALI</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="mb-4">
                            <div class="text-sm-right">
                                <h4 class="text-primary mb-2 mt-md-2">Invoice #{{ $receipt->number }}</h4>
                                <ul class="list list-unstyled mb-0">
                                    <li>Date: <span class="font-weight-semibold">{{ $receipt->created_at}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Tag</th>
                            <th>Unit</th>
                            <th>Total</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($requisitions as $requisition)
                            <tr>
                                <td>
                                    <h6 class="mb-0">{{ $requisition->product_name}}</h6>
                                    <span class="text-muted">{{ $requisition->details}}, {{ $requisition->product_description}}.</span>
                                </td>
                                <td>{{ $requisition->product_tag }}</td>
                                <td>{{ $requisition->product_size }}</td>
                                <td><span class="font-weight-semibold">{{ $requisition->quantity}}</span></td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-body">
                    <div class="d-md-flex flex-md-wrap">
                        <div class="pt-2 mb-3">
                            <h6 class="mb-3">Approve</h6>
                            <div class="mb-3">
                                .................................................................
                            </div>

                            <ul class="list-unstyled text-muted">
                                <li>{{ auth()->user()->name}}</li>
                            </ul>
                        </div>

                        <div class="pt-2 mb-3 wmin-md-400 ml-auto">
                            <h6 class="mb-3">Total Quantity</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th>Total:</th>
                                        <td class="text-right text-primary"><h5
                                                    class="font-weight-semibold">{{
                                            $requisitions->sum('quantity')}}</h5>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section("script")
    @include("layouts.datatable")
    <script type="text/javascript" src="/datatable/custom.js"></script>
@endsection