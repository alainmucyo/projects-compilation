import VueProgressBar from 'vue-progressbar'
import {Form, HasError, AlertError} from 'vform'
import VueSweetalert2 from 'vue-sweetalert2';

import Product from "./components/product/Product";

require('./bootstrap');
import Vue from "vue";
import Added from "./components/Added";
import AddedList from "./components/product/AddedList";
import ReqAdded from "./components/ReqAdded";
import ReqAddedList from "./components/product/ReqAddedList";
import VueHtmlToPaper from 'vue-html-to-paper';
import Sale from "./components/sale/Sale";
import Users from "./components/Users";
import Profile from "./components/Profile";
import Size from "./components/Size";
import Operator from "./components/Operator";

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.use(require('vue-moment'));
const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ],
    styles: [
        '/assets/css/bootstrap.min.css',
    ]
};
Vue.use(VueHtmlToPaper, options);
Vue.filter("currency", (value) => {
    return Number(value).toLocaleString() + " Rwf"
});
let Fire = new Vue();
window.Fire = Fire;
Vue.use(VueSweetalert2);

Vue.use(VueProgressBar, {
    color: '#32b432',
    failedColor: '#e3324c',
    height: '10px'
});
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component("Products", Product);
Vue.component("Added", Added);
Vue.component("AddedList", AddedList);
Vue.component("ReqAdded", ReqAdded);
Vue.component("ReqAddedList", ReqAddedList);
Vue.component("Sale", Sale);
Vue.component("users", Users);
Vue.component("Profile", Profile);
Vue.component("Unit",Size);
Vue.component("operator",Operator);
const app = new Vue({
    el: '#app',
    data: {
        sellerProduct: {},
        oldReq: [],
        clientSales: [],
        client: {},
        products: [],
        machines: [],
        operators: [],
        form: new Form({
            reqQuantity: 0,
            reqDetails: ''
        }),
        pay: new Form({
            receipt: '',
            amount: ''
        }),
        allProductions: {},
        production: new Form({
            actual: '',
            wasted: '',
            expected: '',
            comment: '',
            shift: '',
            machine_id: '',
            product: '',
            operator: ''
        })
    },
    methods: {
        editProduction(production) {
            this.allProductions = production;
            this.production.fill(production);
            this.production.machine_id = production.machine_id;
            this.production.operator_id = production.operator_id;
            this.production.product_id = production.product_id;
            this.loadProducts();
            this.loadMachines();
            this.loadOperators();
            $("#productionEditModal").modal("show");
            console.log(production)
        },
        submitProduction() {
            this.$Progress.start();
            this.production.put("/production/" + this.allProductions.id)
                .then(resp => {
                    this.$Progress.finish();
                    window.location = ""
                })
                .catch(err => {
                    this.$swal("Error", "Error while updating", "error");
                    this.$Progress.fail()
                })
        },
        loadProducts() {
            axios.get("/api/products").then(resp => {
                this.products = resp.data
            })
        },
        loadMachines() {
            axios.get("/api/machines").then(resp => {
                this.machines = resp.data
            })
        }, loadOperators() {
            axios.get("/api/operators").then(resp => {
                this.operators = resp.data
            })
        },

        sales(receipt) {
            console.log(receipt);
            this.$Progress.start();
            this.client = receipt;
            axios.get("/sale/sale/" + receipt.id)
                .then(resp => {
                    this.$Progress.finish();
                    this.clientSales = resp.data

                })
                .catch(err => {
                    this.$Progress.fail();
                    this.$swal("Error", "Error while fetching sales.", "error")
                });
            $("#salesModal").modal("show")
        },
        storePayment() {
            this.pay.put("/sale/payment")
                .then(resp => {
                    this.$swal(
                        "Successfully!",
                        `Client ${resp.data.client.name} 
                        successfully paid ${(this.pay.amount).toLocaleString()} Rwf.
                         There is now left ${(resp.data.required_price - resp.data.paid_price).toLocaleString()} Rwf`,
                        "success"
                    );
                    $("#payModal").modal("hide")
                })
                .catch(err => {
                    if (err.response.status == 400)
                        this.$swal("Not Found", err.response.data, "error");
                    else
                        this.$swal("Error", "Error while adding payment.", "error")
                })
        },
        addToSession(product) {
            Fire.$emit("addToSession", product);
        },

        addToReqSession(product) {

            this.sellerProduct = product;
            $('#testModal').modal('show')

        },
        addRequesition() {
            this.form.put("/requisition/" + this.sellerProduct.id)
                .then(resp => {
                    Fire.$emit("reqAddToSession", resp.data);
                    this.$Progress.finish();
                    $('#testModal').modal('hide')
                })
        },
        submitReq() {
            this.$Progress.start();
            axios.get("/requisition").then(resp => {
                this.oldReq = resp.data;
                if (!resp.data)
                    return this.addRequesition();
                const index = this.oldReq.findIndex(p => p.product_id == this.sellerProduct.id);
                if (index > -1) {
                    this.$Progress.fail();
                    this.$swal("Error", "Product already exists.", "error");
                } else
                    this.addRequesition();
            })
        },
        approveReq(receipt) {
            this.$swal({
                title: 'Are you sure?',
                text: "Approve this requisition list!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Approve !'
            }).then((result) => {
                if (result.value) {
                    this.$Progress.start();
                    axios.post("/approve/" + receipt)
                        .then(() => {
                            this.$htmlToPaper("reqPrint");
                            setTimeout(() => {
                                window.location = ""
                            }, 2000)
                        })
                        .catch(err => {
                            this.$Progress.fail();
                            this.$swal(
                                'Error!',
                                'Problem while approving',
                                'error'
                            )
                        })
                }
            })
        }
    }
});
