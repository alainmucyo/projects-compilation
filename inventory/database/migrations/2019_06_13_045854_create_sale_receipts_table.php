<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleReceiptsTable extends Migration
{

    public function up()
    {
        Schema::create('sale_receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("number");
            $table->bigInteger("required_price");
            $table->bigInteger("paid_price")->default(0);
            $table->integer("client_id");
            $table->string("payment");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_receipts');
    }
}
